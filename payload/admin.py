from django.contrib import admin
from .models import Payload 
# Register your models here.

class PayloadAdmin(admin.ModelAdmin):
    list_display = ('get_id','client', 'employee', 'location','money', 'create_moment','scanned_moment')
    ordering = ('create_moment','scanned_moment')
    search_fields = ('location__name','client__user_account__username','employee__user_account__username','money')
    list_per_page = 10

    def get_id(self, obj):
        return obj.id

    get_id.short_description = 'Payload'


admin.site.register(Payload,PayloadAdmin)
