from .models import Payload
from pointbar_notifications.notifications import send_notification
from rest_framework import viewsets, generics
from .serializers import PayloadSerializer
from django.db.models import Q
from django.shortcuts import get_object_or_404
from rest_framework import generics, status
from rest_framework.response import Response
from .models import Payload
from location.models import Location
from employee.models import Employee
from bar.models import Bar
from subscription.models import Subscription
from point.models import Point
from client.models import Client
from .serializers import PayloadSerializer
from employee.permissions import IsAuthenticatedEmployee
from client.permissions import IsAuthenticatedClient
from rest_framework import generics, viewsets
import datetime
from datetime import date
from rest_framework.pagination import PageNumberPagination

class PayloadAndPointView(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticatedClient]
    serializer_class = PayloadSerializer
        

    def get_queryset(self):
        queryset = []
        number = self.request.GET.get('code')
        payload = Payload.objects.get(qr_code = number)
        queryset.append(payload)

        logged_account=self.request.user
        client=Client.objects.get(user_account=logged_account)

        payload.is_scanned = True
        payload.scanned_moment = datetime.datetime.now()
        payload.client = client

        try:
            point=Point.objects.get(client_id=client.id, location_id = payload.employee.location.id)
            point.quantity=point.quantity + payload.points
            payload.save()
            point.save()
        except:
            location=Location.objects.get(pk=payload.location.id)
            point = Point(
            quantity = payload.points,
            client = client,
            location = location)
            payload.save()
            point.save()

        return queryset

class PayloadView(generics.ListCreateAPIView):
    queryset = Payload.objects.all()
    serializer_class = PayloadSerializer
    


    def post(self, request, *args, **kwargs):
        queryset = []

        st=status.HTTP_201_CREATED
        msg='ok, created'
        
        for data in ['points', 'qr_code', 'create_moment', 'is_scanned', 'money', 'employee']:
            if not data in request.data:
                return Response({}, status=status.HTTP_400_BAD_REQUEST)

        employeeIn=request.data.get('employee')
        employee_id=employeeIn['id']
        
        try: 
            employee = Employee.objects.get(pk = employee_id)  
        except:   
            return Response({}, status=status.HTTP_400_BAD_REQUEST) 

        try:
            subscription = Subscription.objects.get(location_id=employee.location.id, is_active=True)
        except:
            return Response({}, status=status.HTTP_400_BAD_REQUEST)

        try:
            location = Location.objects.get(pk=employee.location.id, is_active=True)
        except:
            return Response({}, status=status.HTTP_400_BAD_REQUEST)

        try: 
            payload = Payload(
                points=request.data.get('points'), 
                qr_code=request.data.get('qr_code'),
                money=request.data.get('money'),
                create_moment= datetime.datetime.now(),
                scanned_moment=None,
                is_scanned=request.data.get('is_scanned'),
                employee=employee,
                location=location, 
                client=None)
            # payload.clean()

            if(employee.control):
                employee.granted_points = employee.granted_points + payload.points
                if(employee.granted_points <= employee.limit_points):
                    employee.save()
                    payload.save()

            else:
                payload.save()
             
        except:
            msg = 'some attributes are invalid'  
            st = status.HTTP_422_UNPROCESSABLE_ENTITY

        return Response(msg, status=st)

class PayloadsByNumber(generics.ListCreateAPIView):
    serializer_class = PayloadSerializer

    def get_queryset(self):
        queryset = []
        number = self.request.GET.get('number')
        
        try:
            payload = Payload.objects.get(qr_code = number)
            queryset.append(payload)
        except:
            msg = 'payload not found'  
            st = status.HTTP_422_UNPROCESSABLE_ENTITY

        return queryset
class SmallPagesPagination(PageNumberPagination):  
    page_size = 10

class PayloadsFilerForBar(generics.ListCreateAPIView):
    serializer_class = PayloadSerializer
    pagination_class = SmallPagesPagination

    def get_queryset(self):
        queryset = []
        location = self.request.GET.get('location')
        employee = self.request.GET.get('employee')
        client = self.request.GET.get('client')
        create_moment_min = self.request.GET.get('create_moment_min')
        create_moment_max = self.request.GET.get('create_moment_max')
        create_moment_exact = self.request.GET.get('create_moment_exact')
        points_min = self.request.GET.get('points_min')
        points_max = self.request.GET.get('points_max')
        points_exact = self.request.GET.get('points_exact')
        gap = int(self.request.GET.get('gap'))

        if self.request.user.authority == 'BAR':
            logged_account=self.request.user
            bar=Bar.objects.get(user_account=logged_account)

            queryset = Payload.objects.all()
            queryset = queryset.filter(employee__bar_field = bar.id, location__is_active = True)

        else:
            logged_account=self.request.user
            clientLogged=Client.objects.get(user_account=logged_account)

            queryset = Payload.objects.all()
            queryset = queryset.filter(client = clientLogged.id, location__is_active = True)

        

        if not (location == "none"):
            queryset = queryset.filter(location__name__iexact=location)
  
        if not (employee == "none"):
            queryset = queryset.filter(Q(employee__name__iexact=employee) | Q(employee__user_account__username=employee))
        
        if ((client.lower() == "not scanned") or (client.lower() == "no escaneado")):
            queryset = queryset.filter(client__isnull = True)

        if not (client == "none"  or (client.lower() == "not scanned") or (client.lower() == "no escaneado")):
            queryset = queryset.exclude(client__isnull = True)
            queryset = queryset.filter(Q(client__name__iexact=client) | Q(client__user_account__username=client))
            # queryset = queryset.exclude(~Q(client__name=client) | ~Q(client__user_account__username=client))

        if not (create_moment_min == "none"):
            format_str = '%Y-%m-%d'
            create_moment_min = datetime.datetime.strptime(create_moment_min, format_str)
            create_moment_min = create_moment_min+datetime.timedelta(hours=gap) 
            date_str = "9999-12-31"
            enddate = datetime.datetime.strptime(date_str, "%Y-%m-%d").date()
            queryset = queryset.filter(create_moment__range=[create_moment_min,enddate])

        if not (create_moment_max == "none"):
            format_str = '%Y-%m-%d'
            create_moment_max = datetime.datetime.strptime(create_moment_max, format_str)
            create_moment_max = create_moment_max+datetime.timedelta(hours=gap) 
            date_str = "1001-12-31"
            startdate = datetime.datetime.strptime(date_str, "%Y-%m-%d").date()
            queryset = queryset.filter(create_moment__range=[startdate,create_moment_max])

        if not (create_moment_exact == "none"):
            format_str = '%Y-%m-%d'
            create_moment_exact = datetime.datetime.strptime(create_moment_exact, format_str) 
            create_moment_exact = create_moment_exact+datetime.timedelta(hours=gap)
            enddate = create_moment_exact+datetime.timedelta(days=1)
            queryset = queryset.filter(create_moment__range=[create_moment_exact,enddate])

        if not ((points_min == "none") or (points_min == "null")):
            queryset = queryset.filter(points__gte = points_min)

        if not ((points_max == "none") or (points_max == "null")):
            queryset = queryset.filter(points__lte = points_max)

        if not ((points_exact == "none") or (points_exact == "null")):
            queryset = queryset.filter(points = points_exact)

        return queryset

class PayloadSend(generics.ListCreateAPIView):
    queryset = Payload.objects.all()
    permission_classes = [IsAuthenticatedEmployee]
    serializer_class = PayloadSerializer
        

    def post(self, request, *args, **kwargs):
        st=status.HTTP_201_CREATED
        msg='ok, created'

        username = self.request.GET.get('username')
        money = self.request.GET.get('money')
        

        logged_account=self.request.user
        employee=Employee.objects.get(user_account=logged_account)

        client = Client.objects.get(user_account__username = username)  

        try:
            subscription = Subscription.objects.get(location_id=employee.location.id, is_active=True)
        except:
            return Response({}, status=status.HTTP_400_BAD_REQUEST)

        location = Location.objects.get(pk=employee.location.id, is_active=True)

        moneyDouble = float(money)
        points = round(moneyDouble * 100)
        
        payload = Payload(
            points=points, 
            qr_code=0,
            money=money,
            create_moment= datetime.datetime.now(),
            scanned_moment=datetime.datetime.now(),
            is_scanned=True,
            employee=employee,
            location=location, 
            client=client)

        if(employee.control):
            employee.granted_points = employee.granted_points + payload.points
            if(employee.granted_points <= employee.limit_points):
                employee.save()
                payload.save()
                try:
                    point=Point.objects.get(client_id=client.id, location_id = payload.employee.location.id)
                    point.quantity=point.quantity + payload.points
                    point.save()
                    send_notification(client.user_account, 'send_points' , 'send_points_description1')
                except:
                    location=Location.objects.get(pk=payload.location.id)
                    point = Point(
                    quantity = payload.points,
                    client = client,
                    location = location)
                    point.save()
                    send_notification(client.user_account, 'send_points' , 'send_points_description1___' + str(payload.points) + 'send_points_description2___' + str(location.name))

        else:
            payload.save()
            try:
                point=Point.objects.get(client_id=client.id, location_id = payload.employee.location.id)
                point.quantity=point.quantity + payload.points
                point.save()
                send_notification(client.user_account, 'send_points' , 'send_points_description1' + str(payload.points) + 'send_points_description2' + str(location.name))
            except:
                location=Location.objects.get(pk=payload.location.id)
                point = Point(
                quantity = payload.points,
                client = client,
                location = location)
                point.save()
                send_notification(client.user_account, 'send_points' , 'send_points_description1' + str(payload.points) + 'send_points_description2' + str(location.name))
                          


        return Response(msg, status=st)

        