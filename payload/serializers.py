from .models import Payload
from .models import Employee
from location.serializers import LocationSerializer
from users.serializers import UserAccountDetailsSerializer
from bar.serializers import BarSerializer
from client.serializers import ClientSerializer
from rest_framework import serializers

class employeeSerializer(serializers.HyperlinkedModelSerializer):
    location = LocationSerializer(many=False)
    bar_field = BarSerializer(many=False)
    user_account = UserAccountDetailsSerializer(many=False)

    class Meta:
        model = Employee
        fields = ('id', 'name', 'is_banned', 'inscriptionDate', 'user_account', 'is_working', 'location', 'bar_field')

class PayloadSerializer(serializers.HyperlinkedModelSerializer):
    employee = employeeSerializer(many=False)
    location = LocationSerializer(many=False)
    client = ClientSerializer(many=False)

    class Meta:
        model = Payload
        fields = ['id', 'points', 'qr_code', 'create_moment', 'scanned_moment', 'is_scanned', 'money', 'employee', 'location', 'client']

