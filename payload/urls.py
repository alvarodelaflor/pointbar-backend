from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()

urlpatterns = [
    path('api/payload/', views.PayloadView.as_view(), name='PayloadView'),
    path('api/payload/send/', views.PayloadSend.as_view(), name='PayloadSend'),
    path('api/payload/bar/', views.PayloadsFilerForBar.as_view(), name='PayloadsFilerForBar'),
    path('api/payload-point/', views.PayloadAndPointView.as_view(), name='PayloadAndPointView'),
    # path('api/payload/<int:payload_id>/', views.PayloadUpdate.as_view(), name='payload'),
    # path('api/payload_per_location/', views.PayloadsActivesPerLocation.as_view(), name='locationpayload'),
    # path('api/payload/', views.PayloadPerId.as_view(), name='payloadPerId')
]
