from django.db import models
from django.utils import timezone
from employee.models import Employee
from client.models import Client
from location.models import Location

class Payload(models.Model):
    points = models.PositiveIntegerField(blank=False)
    qr_code = models.CharField(max_length=200, blank=False)
    money = models.FloatField(blank=False)
    create_moment = models.DateTimeField(blank=False, null=False)
    scanned_moment = models.DateTimeField(null=True, blank=True)
    is_scanned = models.BooleanField(blank=False)
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE, null=False,related_name='employeeRelated')
    location = models.ForeignKey(Location, on_delete=models.CASCADE, null=False, related_name='locationRelated')
    client = models.ForeignKey(Client, blank=True, null=True, on_delete=models.CASCADE, related_name='clientRelated')

    def __str__(self):
       return 'Client: {0}, Employee: {1}'.format(self.client, self.employee)
