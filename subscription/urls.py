from django.urls import include, path
from rest_framework import routers
from subscription import views

router = routers.DefaultRouter()

urlpatterns = [
    path('api/subscription/', views.SubscriptionView.as_view(), name='subscriptions'),
    path('api/subscription/<id>/', views.SubscriptionUpdate.as_view(), name='subscription'),
    path('api/subscription/bar/vip/', views.SubscriptionGetVipForLoggedBar.as_view(), name='subscriptionVipBar'),
    path('api/subscription/pagination', views.ListPaginatedSubscriptionsAPIView.as_view(), name='pagination'),
    path('api/subscription/bar', views.SubscriptionLogged.as_view(), name='subscriptionBar'),
    path('api/subscription/vip', views.VipSubscriptions.as_view(), name='vip_subscription'),
    path('api/subscription/normal', views.NormalSubscriptions.as_view(), name='normal_subscription'),
    path('api/subscription/cancel', views.subscription_cancellation, name='subscription_cancel')
]