from django.contrib import admin
from .models import Subscription

class SubscriptionAdmin(admin.ModelAdmin):
    list_display = ('get_id', 'get_membership','get_location', 'moment','is_active')
    ordering = ('moment',)
    search_fields = ('membership__membership_type','location__name','membership__titleE')
    list_filter = ('is_active','membership__membership_type')
    list_per_page = 10

    def get_id(self, obj):
        return obj.id

    get_id.short_description = 'Subscription'

    def get_membership(self, obj):
        return obj.membership

    get_membership.short_description = 'Membership'
    get_membership.admin_order_field = 'subscription__membership'

    def get_location(self, obj):
        return obj.location

    get_location.short_description = 'Location'
    get_location.admin_order_field = 'Subscription__location'

admin.site.register(Subscription,SubscriptionAdmin)