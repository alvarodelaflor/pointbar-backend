from django.db import models
from membership.models import Membership
from location.models import Location
from django.utils import timezone
from datetime import datetime
from django.utils import timezone
from django.core.exceptions import ValidationError

class Subscription(models.Model):   
    membership = models.ForeignKey(Membership, on_delete=models.CASCADE, related_name='membershipSubscription')
    location = models.ForeignKey(Location, on_delete=models.CASCADE, related_name='membershipLocation')
    moment = models.DateField(('Momment Created'),blank=False, null=False,default=timezone.now)   
    paypal_subscription_id = models.TextField(null=False, default=None) 
    is_active = models.BooleanField(default=True)
    renovate = models.BooleanField(default=True)

    def __str__(self):
       return 'Membership: {0}, Location: {1}'.format(self.membership, self.location)

    def clean(self):

        if self.is_active:
            try:
                subscription = Subscription.objects.get(location_id=self.location.id, is_active=True)
            except:
                subscription=None

            if not(subscription==None):
                raise ValidationError('You can not have 2 active subscription')

                
