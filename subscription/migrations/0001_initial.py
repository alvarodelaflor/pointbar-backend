# Generated by Django 3.0.3 on 2020-03-16 14:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('membership', '0001_initial'),
        ('location', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Subscription',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bank_account', models.CharField(blank='false', max_length=100)),
                ('expiration_date', models.DateField(verbose_name='Expiration Date')),
                ('is_active', models.BooleanField(default=True)),
                ('renovate', models.BooleanField(default=False)),
                ('location', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='location.Location')),
                ('membership', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='membership.Membership')),
            ],
        ),
    ]
