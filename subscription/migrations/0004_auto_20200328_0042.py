# Generated by Django 3.0.3 on 2020-03-28 00:42

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('subscription', '0003_auto_20200328_0031'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='subscription',
            name='expiration_date',
        ),
        migrations.AddField(
            model_name='subscription',
            name='moment',
            field=models.DateField(default=django.utils.timezone.now, verbose_name='Expiration Date'),
        ),
    ]
