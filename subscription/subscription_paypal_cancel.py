import base64
import json

import requests

from django.conf import settings


def paypal_cancellation(paypal_subscription_id):

    client_id = settings.PAYPAL_CLIENT_ID

    if client_id:
        secret = settings.PAYPAL_SECRET
        token = client_id + ':' + secret

        encoded_bytes = base64.b64encode(token.encode('utf-8'))
        encoded_token = str(encoded_bytes, 'utf-8')

        url = 'https://api.sandbox.paypal.com/v1/billing/subscriptions/' + \
            paypal_subscription_id + '/cancel'
        data = {'reason': 'Not satisfied with the service'}
        headers = {'Content-Type': 'application/json',
                   'Authorization': 'Basic ' + encoded_token}

        paypal_response = requests.post(
            url, data=json.dumps(data), headers=headers)

        return paypal_response

    else:
        pass
