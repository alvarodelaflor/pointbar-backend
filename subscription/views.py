import base64
import json
from datetime import date

import requests

from bar.models import Bar
from bar.permissions import IsAuthenticatedBar
from django.conf import settings
from django.core.exceptions import ValidationError
from django.shortcuts import get_object_or_404
from location.models import Location
from membership.models import Membership
from pointbar_notifications.notifications import send_notification
from rest_framework import generics, status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

from .models import Subscription
from .serializers import SubscriptionSerializer
from .subscription_paypal_cancel import paypal_cancellation


@api_view(["POST"])
@permission_classes((IsAuthenticatedBar,))
def subscription_cancellation(request):
    data = json.loads(json.dumps(request.data))
    subscription_id = data['id']

    subscription = Subscription.objects.get(id=subscription_id)

    paypal_response = paypal_cancellation(subscription.paypal_subscription_id)
    
    msg = 'Could not cancel the susbcription'
    st = status.HTTP_422_UNPROCESSABLE_ENTITY

    if paypal_response.status_code == 204:
        try:
            msg = 'Subscription cancel success'
            st = status.HTTP_204_NO_CONTENT
            subscription.is_active = False
            subscription.renovate = False
            subscription.save()
        except:
            pass

    return Response(msg, status=st)



class VipSubscriptions(generics.ListCreateAPIView):
    serializer_class = SubscriptionSerializer
    queryset=[]

    def get_queryset(self):
        queryset=Subscription.objects.filter(is_active=True, membership__membership_type='VIP')
        return queryset

class NormalSubscriptions(generics.ListCreateAPIView):
    serializer_class = SubscriptionSerializer
    queryset=[]

    def get_queryset(self):
        queryset=Subscription.objects.filter(is_active=True, membership__membership_type='Normal')
        return queryset                  


class SubscriptionView(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticatedBar]
    
    def post(self, request, *args, **kwargs):

        st = status.HTTP_201_CREATED
        msg = 'ok, created'

        for data in ['renovate', 'location', 'membership', 'paypal_subscription_id']:
            if not data in request.data:
                return Response({}, status=status.HTTP_400_BAD_REQUEST)

        locationIn = request.data.get('location')
        location_id = locationIn['id']

        try:
            location = Location.objects.get(pk=location_id)
        except:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

        membershipIn = request.data.get('membership')
        membership_id = membershipIn['id']

        try:
            membership = Membership.objects.get(pk=membership_id)
        except:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

        today = date.today()
        try:
            subscription = Subscription(
                membership=membership,
                location=location,
                moment=today,
                is_active=True,
                renovate=request.data.get('renovate'),
                paypal_subscription_id=request.data.get('paypal_subscription_id'))

            subscription.clean()
            subscription.save()

            send_notification(self.request.user, 'notification_subscription_create', 'notification_subscription_create_description')
         

        except:
            msg = 'some of atributes are invalid'
            st = status.HTTP_422_UNPROCESSABLE_ENTITY

        return Response(msg, status=st)


class SubscriptionUpdate(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = SubscriptionSerializer
    queryset = Subscription.objects.all()
    permission_classes = [IsAuthenticatedBar]
    lookup_field = 'id'

    def put(self, request, id, *args, **kwars):

        subscription = get_object_or_404(Subscription, pk=id)
        st = status.HTTP_200_OK
        msg = 'updated'

        try:
            subscription.renovate = request.data.get('renovate')
            subscription.save()
        except:
            msg = 'some of atributes are invalid'
            st = status.HTTP_422_UNPROCESSABLE_ENTITY

        return Response(msg, status=st)

    def delete(self, request, id, *args, **kwars):
        subscription = get_object_or_404(Subscription, pk=id)
        st = status.HTTP_200_OK
        msg = 'deleted'

        try:
            subscription.renovate = False
            subscription.is_active = False
            subscription.save()
        except:
            msg = 'cannot delete'
            st = status.HTTP_409_CONFLICT

        return Response(msg, status=st)

class SubscriptionGetVipForLoggedBar(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticatedBar]
    serializer_class = SubscriptionSerializer
        

    def get_queryset(self):
        queryset = []

        logged_account=self.request.user
        bar=Bar.objects.get(user_account=logged_account)

        try:
            queryset = Subscription.objects.filter(membership__membership_type = 'VIP', location__bar = bar.id)
        except:
            pass
        

        return queryset
class SubscriptionLogged(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticatedBar]
    serializer_class = SubscriptionSerializer
    
    def get_queryset(self):
        queryset=[]
        try:
            accountId=self.request.user.id
            bar=Bar.objects.get(user_account=accountId)
            queryset = Subscription.objects.filter(location__bar= bar,is_active=True)
        except:
            raise ValidationError('You can not see these subscription')
        return queryset         

class SmallPagesPagination(PageNumberPagination):  
    page_size = 8
class ListPaginatedSubscriptionsAPIView(generics.ListAPIView):
    """
    List active sales
    """
    pagination_class = SmallPagesPagination
    permission_classes = [IsAuthenticatedBar]
    serializer_class = SubscriptionSerializer

    def get_queryset(self):
        user = self.request.user
        subscriptions = []
        if(user.is_anonymous == False and user.authority == 'BAR'):
            bar_id = Bar.objects.get(user_account=self.request.user).id
            print(bar_id)
            locations = Location.objects.filter(bar=bar_id)
            print(locations)
            for location in locations:
                subs = Subscription.objects.filter(location=location)
                subscriptions += subs
        return subscriptions
