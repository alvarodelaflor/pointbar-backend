from django.test import TestCase
from django.urls import include, path, reverse
from rest_framework.test import APITestCase, URLPatternsTestCase
from rest_framework import status
from users.models import UserAccount
from bar.models import Bar
from rest_framework.test import APIClient
from users.authorities import Authority
from location.models import Location
from membership.models import Membership
from .models import Subscription
from datetime import date


class API_subscription_test(APITestCase, URLPatternsTestCase):
    urlpatterns = [path('', include('pointbar.urls')),]

    def setUp(self):

        super().setUp()

        user = UserAccount.objects.create_user('JuanBar', 'Ejemplo1', 'juan@gmail.com', Authority.BAR.value)
        user.is_active=True
        user.save()
        UserAccount.objects.create_user('system', 'Ejemplo1', 'system@gmail.com', Authority.BAR.value)
        

        bar = Bar(
            name = 'JuanBarSubscription',
            is_banned = False,
            user_account = user,
            phone= '672108962')
        bar.save()

        location = Location(
                name='location_test',
                description='description_test',
                email='testSubscription@gmail.com',
                website='https://example.com',
                logo='https://example.com',
                latitude=3,
                longitude=3,
                street='exaple_street',
                city='exaple_city',
                state='exaple_state',
                postal_code=41089,
                country='exaple_country',
                is_active=True,
                bar=bar)
        location.save()

        membership = Membership(
                titleS='title_test',
                descriptionS='description_test',
                titleE='title_test',
                descriptionE='description_test',
                price=50,
                membership_type='VIP',
                subscriptionId=3
                )
        membership.save()

        
    def tearDown(self):
        super().tearDown()

    def client_logged(self,json_user_data):

        url = reverse('token_obtain_pair')
        response = self.client.post(url,json_user_data, format='json')
        token=response.data['access']
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)
        return client

    def test_vip_subscriptions(self):

        client = APIClient()
        url = reverse('vip_subscription')
        response = client.get(url, data={'format': 'json'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_normal_subscriptions(self):

        client = APIClient()
        url = reverse('normal_subscription')
        response = client.get(url, data={'format': 'json'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_update_delete_subscription(self):

        user_data= {
                "username": "JuanBar",
                "password": "Ejemplo1"
              }
             
        client= API_subscription_test.client_logged(self,user_data)
        
        #create the subscription
        url = reverse('subscriptions')
        location=Location.objects.get(email='testSubscription@gmail.com')
        membership=Membership.objects.get(membership_type='VIP')

        data = {
            'moment': date.today(),
            'is_active': True,
            'renovate': True,
            'paypal_subscription_id': 234323333,
            'membership' : {'id': membership.id,},
            'location' : {'id': location.id,}
        }

        response = client.post(url,data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        subscription_saved=Subscription.objects.get(paypal_subscription_id=234323333)
        self.assertEqual(location.id,subscription_saved.location.id)

        #update the subscription

        kwargs = {
            'id': subscription_saved.id,
        }

        url = reverse('subscription', kwargs=kwargs)
        data = {
            'moment': date.today(),
            'is_active': True,
            'renovate': True,
            'paypal_subscription_id': 234323333,
            'membership' : {'id': membership.id,},
            'location' : {'id': location.id,}
        }

        response = client.put(url,data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        #delete sale

        kwargs = {
            'id': subscription_saved.id,
        }
        url = reverse('subscription', kwargs=kwargs)

        response = client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        subscription_deleted=Subscription.objects.get(paypal_subscription_id=234323333)
        self.assertEqual(False,subscription_deleted.is_active)

    def test_get_vip_logged_bar(self):

        user_data= {
                "username": "JuanBar",
                "password": "Ejemplo1"
              }
             
        client= API_subscription_test.client_logged(self,user_data)
        
        url = reverse('subscriptionVipBar')
        response = client.get(url, data={'format': 'json'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_logged(self):

        user_data= {
                "username": "JuanBar",
                "password": "Ejemplo1"
              }
             
        client= API_subscription_test.client_logged(self,user_data)
        
        url = reverse('subscriptionBar')
        response = client.get(url, data={'format': 'json'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)                     