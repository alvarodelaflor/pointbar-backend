from .models import Subscription
from rest_framework import serializers
from location.models import Location
from membership.models import Membership
from membership.serializers import MembershipSerializer
from location.serializers import LocationSerializer




class SubscriptionSerializer(serializers.HyperlinkedModelSerializer):
    membership = MembershipSerializer(many=False)
    location = LocationSerializer(many=False)
    
    class Meta:
        model = Subscription
        fields = ('id','membership','location','moment','is_active', 'renovate', 'paypal_subscription_id')
