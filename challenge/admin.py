from django.contrib import admin
from .models import Challenge

class ChallengeAdmin(admin.ModelAdmin):
    list_display = ('title', 'number_points', 'start_date','expiration_date','tipe')
    ordering = ('start_date','expiration_date')
    search_fields = ('title','tipe')
    list_filter = ('location','is_active',)
    list_per_page = 10



admin.site.register(Challenge,ChallengeAdmin)