from payload.models import Payload
from sale.models import Sale
from employee.models import Employee
from point.models import Point
from datetime import date
from datetime import datetime
from django.test import TestCase
from django.urls import include, path, reverse
from rest_framework.test import APITestCase, URLPatternsTestCase
from rest_framework import status
from users.models import UserAccount
from bar.models import Bar
from challenge.models import Challenge
from location.models import Location
from membership.models import Membership
from subscription.models import Subscription
from client.models import Client
from datetime import date
from rest_framework.test import APIClient
from users.authorities import Authority



class API_challenge_test(APITestCase, URLPatternsTestCase):

    urlpatterns = [path('', include('pointbar.urls')),]

    def setUp(self):
        super().setUp()

        user = UserAccount.objects.create_user('JuanBar', 'Ejemplo1', 'juan@gmail.com', Authority.BAR.value)
        user.is_active=True
        user.save()
        

        bar = Bar(
            name = 'JuanBar',
            is_banned = False,
            user_account = user,
            phone= '672108962')
        bar.save()
        
        
        
        user_data= {
                "username": "JuanBar",
                "password": "Ejemplo1"
              }
             
        client= API_challenge_test.client_logged(self,user_data)

        location = Location(
                name='location_test',
                description='description_test',
                email='test@gmail.com',
                website='https://example.com',
                logo='https://example.com',
                latitude=3,
                longitude=3,
                street='exaple_street',
                city='exaple_city',
                state='exaple_state',
                postal_code=41089,
                country='exaple_country',
                is_active=True,
                bar=bar)

        location.save()
        
        membership = Membership(
                titleS='title_test',
                descriptionS='description_test',
                titleE='title_test',
                descriptionE='description_test',
                price=50,
                membership_type='VIP',
                subscriptionId=3
                )
        membership.save()

        subscription = Subscription(
                membership=membership,
                location=location,
                moment=date.today(),
                is_active=True,
                renovate=True,
                paypal_subscription_id=23432)

        subscription.save()


        user_cliente = UserAccount.objects.create_user('PepePointBar', 'Ejemplo1', 'pepe@gmail.com', Authority.CLIENT.value)
        user_cliente.is_active=True
        user_cliente.save()
        user_employee = UserAccount.objects.create_user('JuanEmployee1', 'Ejemplo1', 'emplojuan1@gmail.com', Authority.EMPLOYEE.value)
        user_employee.is_active=True
        user_employee.save()

        format_str = '%Y-%m-%d'
        birth = datetime.strptime('2000-11-11', format_str)
        cliente = Client(
            name = 'Pepe',
            is_banned = False,
            user_account = user_cliente,
            surname = 'Ripoll',
            birth_date = birth.date(),
            phone= '672108962')
        
        cliente.save()
        
        employee = Employee(
            name = 'JuanEmployeePayload1',
            is_banned = False,
            user_account = user_employee,
            is_working= True,
            location=location,
            bar_field=bar,
            control=True,
            limit_points=1000000,
            granted_points=0)
            
        employee.save()

        point = Point(
            quantity = 100000,
            client = cliente,
            location = location
        )
        point.save()

        payload = Payload(
            points = 10,
            qr_code = 999,
            money = 100,
            create_moment = datetime.now(),
            scanned_moment = datetime.now(),
            is_scanned =True,
            employee = employee,
            client = cliente,
            location = location
        )
        payload.save()

        start = datetime.strptime('9997-11-11', format_str)
        end = datetime.strptime('9999-11-11', format_str)
        sale = Sale(
            title = 'sale_test_exchange',
            description = 'description_test',
            number_points = '33',
            start_date = start,
            expiration_date = end,
            location = location
        )
        sale.save()

        format_str = '%Y-%m-%d'
        start_1 = datetime.strptime('2000-11-11', format_str)
        end_1= datetime.strptime('2080-11-11', format_str)
        challenge = Challenge(
            title = 'challenge_test_claimed',
            description= 'description_test',
            number_points= 30,
            start_date = start_1.date(),
            tipe='F',
            money_spent = 10,
            number=9,
            expiration_date=end_1.date(),
            location= location
        )
        challenge.save()

       
        
        challenge = Challenge(
            title = 'challenge_test_claimed_C',
            description= 'description_test',
            number_points= 10,
            start_date = start_1.date(),
            tipe='C',
            money_spent = 10,
            number=1,
            expiration_date=end_1.date(),
            location= location
        )
        challenge.save()
        
        challenge = Challenge(
            title = 'challenge_test_claimed_D',
            description= 'description_test',
            number_points= 11,
            start_date = start_1.date(),
            tipe='D',
            money_spent = 10,
            number=1,
            expiration_date=end_1.date(),
            location= location
        )
        challenge.save()


        challenge = Challenge(
            title = 'challenge_test_claimed_CM',
            description= 'description_test',
            number_points= 12,
            start_date = start_1.date(),
            tipe='CM',
            money_spent = 10,
            number=1,
            expiration_date=end_1.date(),
            location= location
        )
        challenge.save()
        
        

    def tearDown(self):
        super().tearDown()

    def client_logged(self,json_user_data):

        url = reverse('token_obtain_pair')
        response = self.client.post(url,json_user_data, format='json')
        token=response.data['access']
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)
        return client

    def test_get_all(self):

        client = APIClient()
        url = reverse('challenges')
        response = client.get(url, data={'format': 'json'})
        self.assertEqual(response.status_code, status.HTTP_200_OK) 

   

    def test_create_update_delete_challenge(self):

        user_data= {
                "username": "JuanBar",
                "password": "Ejemplo1"
              }
             
        client= API_challenge_test.client_logged(self,user_data)
        
        #create the challenge
        url = reverse('challenges')
        location=Location.objects.get(email='test@gmail.com')

        data = {
            'title': 'challenge_test',
            'description': 'description_test',
            'number_points': '33',
            'start_date': '9998-02-02',
            'tipe':'IM',
            'money_spent':'10',
            'number':'1',
            'expiration_date' : '9999-03-03',
            'location' : {'id':location.id,}
        }

        response = client.post(url,data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        challenge_saved = Challenge.objects.get(title='challenge_test')
        self.assertEqual(33,challenge_saved.number_points)

        #get one challenge

        kwargs = {
            'id': challenge_saved.id,
        }
        url = reverse('challenge', kwargs=kwargs)

        response = client.get(url, data={'format': 'json'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        

    def test_get_historical(self):

        user_data= {
                "username": "JuanBar",
                "password": "Ejemplo1"
              }
             
        client= API_challenge_test.client_logged(self,user_data)
        location=Location.objects.get(email='test@gmail.com')
        kwargs = {
            'id': location.id,
        }

        url = reverse('historical_location_challenge', kwargs=kwargs)
        response = client.get(url, data={'format': 'json'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)


    def test_get_future_challenge(self):

        user_data= {
                "username": "JuanBar",
                "password": "Ejemplo1"
              }
             
        client= API_challenge_test.client_logged(self,user_data)
        location=Location.objects.get(email='test@gmail.com')
        kwargs = {
            'id': location.id,
        }

        url = reverse('future_location_challenge', kwargs=kwargs)
        response = client.get(url, data={'format': 'json'})
        self.assertEqual(response.status_code, status.HTTP_200_OK) 


    def test_get_now_challenges(self):

        user_data= {
                "username": "JuanBar",
                "password": "Ejemplo1"
              }
             
        client= API_challenge_test.client_logged(self,user_data)
        location=Location.objects.get(email='test@gmail.com')
        kwargs = {
            'id': location.id,
        }

        url = reverse('paginated_challenge', kwargs=kwargs)
        response = client.get(url, data={'format': 'json'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)          
    
    def test_claim_challenges(self):

        

        user_data= {
                "username": "PepePointBar",
                "password": "Ejemplo1"
              }
             
        client= API_challenge_test.client_logged(self,user_data)
        challenge_to_claimed = Challenge.objects.get(title='challenge_test_claimed')
        
        kwargs = {

            'id': challenge_to_claimed.id,
        }

        url = reverse('claim_challenge', kwargs=kwargs)
        response = client.get(url, data={'format': 'json'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)



    def test_claim_challenges_tipe_C(self):

        

        user_data= {
                "username": "PepePointBar",
                "password": "Ejemplo1"
              }
             
        client= API_challenge_test.client_logged(self,user_data)
        challenge_to_claimed = Challenge.objects.get(number_points=10)
        kwargs = {

            'id': challenge_to_claimed.id,
        }

        url = reverse('claim_challenge', kwargs=kwargs)
        
        response = client.get(url, data={'format': 'json'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)


    def test_claim_challenges_tipe_D(self):

        

        user_data= {
                "username": "PepePointBar",
                "password": "Ejemplo1"
              }
             
        client= API_challenge_test.client_logged(self,user_data)
        challenge_to_claimed = Challenge.objects.get(number_points=11)
        kwargs = {

            'id': challenge_to_claimed.id,
        }

        url = reverse('claim_challenge', kwargs=kwargs)
        
        response = client.get(url, data={'format': 'json'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)



    def test_claim_challenges_tipe_CM(self):

        

        user_data= {
                "username": "PepePointBar",
                "password": "Ejemplo1"
              }
             
        client= API_challenge_test.client_logged(self,user_data)
        challenge_to_claimed = Challenge.objects.get(number_points=12)
        kwargs = {

            'id': challenge_to_claimed.id,
        }

        url = reverse('claim_challenge', kwargs=kwargs)
        
        response = client.get(url, data={'format': 'json'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)