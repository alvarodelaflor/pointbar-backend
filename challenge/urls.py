from django.urls import include, path
from rest_framework import routers
from . import views



urlpatterns = [
    path('api/challenges', views.ChallengeView.as_view(), name='challenges'),
    path('api/challenge/<id>/', views.ChallengeUpdate.as_view(), name='challenge'),
    path('api/claim_challenge/<id>/', views.ClaimChallenge.as_view(), name='claim_challenge'),
    path('api/historicalchallengesperlocation/<id>/', views.HistoricalChallengesPerLocation.as_view(), name='historical_location_challenge'),
    path('api/futurechallenges/<id>/', views.FutureChallengesPerLocation.as_view(), name='future_location_challenge'),
    path('api/paginatedchallenges/<id>/', views.ListPaginatedChallengesAPIView.as_view(), name='paginated_challenge'),
    path('api/paginatedfuturechallenges/<id>/', views.ListPaginatedFutureChallengesAPIView.as_view(), name='paginated_future_challenge'),
    path('api/paginatedhistoricalchallenges/<id>/', views.ListPaginatedHistoricalChallengesAPIView.as_view(), name='paginated_historical_challenge')]