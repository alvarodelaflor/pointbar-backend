from datetime import datetime
from django.conf import settings
from django.utils import timezone
from django.shortcuts import get_object_or_404
from rest_framework import generics, status
from rest_framework.response import Response
from .models import Challenge
from location.models import Location
from .serializers import ChallengeSerializer
from bar.permissions import IsAuthenticatedBar
from completed_challenge.models import Completed_challenge
from completed_challenge.serializers import completed_challengeSerializer
from datetime import date
from datetime import timedelta
from payload.models import Payload
from exchange.models import Exchange
import pytz
from client.models import Client
from employee.models import Employee
from point.models import Point
from client.permissions import IsAuthenticatedClient
from bar.models import Bar
from django.core.exceptions import ValidationError
from datetime import date
from datetime import datetime
from rest_framework.generics import RetrieveAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.pagination import PageNumberPagination


class ChallengeView(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticatedBar]
    queryset = Challenge.objects.filter(is_active=True)
    serializer_class = ChallengeSerializer    

    def get(self, request, *args, **kwargs):
        self.serializer_class = ChallengeSerializer
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):

        st=status.HTTP_201_CREATED
        msg='ok, created'
        
        for data in ['title', 'description', 'number_points', 
                    'start_date','expiration_date','tipe','number','money_spent','location']:
            if not data in request.data:
                return Response({}, status=status.HTTP_400_BAD_REQUEST)

        
        locationIn=request.data.get('location')
        location_id=locationIn['id']

        try: 
            accountId=request.user.id
            bar=Bar.objects.get(user_account=accountId)
            locationBar= Location.objects.get(pk=location_id,is_active=True)
            if not(locationBar.bar.id==bar.id):
                return Response({}, status=status.HTTP_401_UNAUTHORIZED)
        except:   
            return Response({}, status=status.HTTP_401_UNAUTHORIZED)
        
        try: 
            location = Location.objects.get(pk = location_id)  
        except:   
            return Response({}, status=status.HTTP_404_NOT_FOUND) 
        

        try:
            format_str = '%Y-%m-%d'
            datetimeStart = datetime.strptime(request.data.get('start_date'), format_str)
            datetimeExpiration = datetime.strptime(request.data.get('expiration_date'), format_str) 
            challenge = Challenge(
                title=request.data.get('title'), 
                description=request.data.get('description'),
                number_points=request.data.get('number_points'),
                photo=request.data.get('photo'),
                is_active=True,
                start_date= datetimeStart.date(),
                expiration_date=datetimeExpiration.date(),
                tipe=request.data.get('tipe'),
                money_spent=request.data.get('money_spent'),
                number=request.data.get('number'),
                location=location)

            if challenge.tipe=='F':
                challenge.money_spent=None
                challenge.number=None
                   
            elif challenge.tipe=='C' or challenge.tipe=='D':
                challenge.money_spent=None

            elif challenge.tipe=='IM' or challenge.tipe=='AM':
                challenge.number=None

            challenge.clean()
            challenge.save()
             
        except:
            msg = 'some of atributes are invalid'  
            st = status.HTTP_422_UNPROCESSABLE_ENTITY

        return Response(msg, status=st)


class ChallengeUpdate(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ChallengeSerializer
    queryset = Challenge.objects.all()
    lookup_field = 'id'
    #SHOW
    def get(self, request, id, *args, **kwargs):
        self.serializer_class = ChallengeSerializer
        queryset = Challenge.objects.get(id = id)
        today=date.today()
        if not(queryset.is_active):
            raise ValidationError('You can not see this challenge')
        try:
            isBar=self.request.user.authority == 'BAR'
        except:
            isBar=False

        if (queryset.expiration_date<today) and (isBar):
            accountId=self.request.user.id
            bar=Bar.objects.get(user_account=accountId)
            if not(queryset.location.bar.id==bar.id):
                raise ValidationError('You can not see this challenge')
        else:
            if (queryset.expiration_date<today):
                raise ValidationError('You can not see this challenge')
        
        return super().get(request, *args, **kwargs) 


class SmallPagesPagination(PageNumberPagination):  
    page_size = 6


class HistoricalChallengesPerLocation(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticatedBar]
    pagination_class = SmallPagesPagination
    serializer_class = ChallengeSerializer
    lookup_url_kwarg = "id"
    queryset=[]

    def get_queryset(self):
        locationId = self.kwargs.get(self.lookup_url_kwarg)
        try:
            accountId=self.request.user.id
            bar=Bar.objects.get(user_account=accountId)
            locationBar= Location.objects.get(pk=locationId,is_active=True)
            if not(locationBar.bar.id==bar.id):
                raise ValidationError('You can not see this historical')
            today=date.today()
            ayer=today-timedelta(days=1)
            date_str = "1900-01-11"
            oldDate = datetime.strptime(date_str, "%Y-%m-%d").date()
            queryset = Challenge.objects.filter(location_id = locationId,is_active=True,expiration_date__range=[oldDate,ayer])
        except:
            raise ValidationError('You can not see this historical')
        return queryset         

class FutureChallengesPerLocation(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticatedBar]
    pagination_class = SmallPagesPagination
    serializer_class = ChallengeSerializer
    lookup_url_kwarg = "id"
    queryset=[]

    def get_queryset(self):
        locationId = self.kwargs.get(self.lookup_url_kwarg)
        try:
            accountId=self.request.user.id
            bar=Bar.objects.get(user_account=accountId)
            locationBar= Location.objects.get(pk=locationId,is_active=True)
            if not(locationBar.bar.id==bar.id):
                raise ValidationError('You can not see these futures challenges')
            today=date.today()
            mañana=today+timedelta(days=1)
            date_str = "9999-12-31"
            enddate = datetime.strptime(date_str, "%Y-%m-%d").date()
            queryset = Challenge.objects.filter(location_id = locationId,is_active=True,start_date__range=[mañana,enddate])
        except:
            raise ValidationError('You can not see these futures challenges')
        return queryset  




def asignaPuntos(client,locationId,pointsChallenge):
    try:
        point=Point.objects.get(client_id=client.id,location_id=locationId)
        point.quantity=point.quantity+pointsChallenge
        point.save()
    except:
        location=Location.objects.get(pk=locationId)
        point = Point(
        quantity = pointsChallenge,
        client = client,
        location = location)
        point.save()

def actualiza_completed_challenge(client,challenge,completed_challenge_BD):
    if completed_challenge_BD==None:
        completed_challenge = Completed_challenge(
        last_challenge_moment = datetime.now(),
        count = 1,
        challenge = challenge,
        client = client)
        completed_challenge.save()
        res=completed_challenge
                    
    else:
        completed_challenge_BD.count=completed_challenge_BD.count+1
        completed_challenge_BD.last_challenge_moment=datetime.now()
        res=completed_challenge_BD.save()
        res=completed_challenge_BD

    asignaPuntos(client,challenge.location.id,challenge.number_points)
    return res

def days_between(d1, d2):
    return abs(d2 - d1).days   

class ClaimChallenge(APIView):
    permission_classes = [IsAuthenticatedClient]
    
    def get(self, request,id, format=None):
        challengeId = id
        challenge = Challenge.objects.get(pk = challengeId)
        accountId=self.request.user.id
        client=Client.objects.get(user_account=accountId)
        userId=client.id
        today=date.today()
        

        if not(challenge.is_active):
            msg = 'error_challenge_invalid _challenge'
            return Response(msg,status=status.HTTP_400_BAD_REQUEST)
        if challenge.expiration_date < today:
            msg = 'error_challenge_expire'
            return Response(msg,status=status.HTTP_400_BAD_REQUEST)
        if challenge.start_date > today:
            msg = 'error_challenge_not_started'
            return Response(msg,status=status.HTTP_400_BAD_REQUEST)

        try:
            completed_challenge_BD= Completed_challenge.objects.get(client_id=userId, challenge_id=challengeId)
        except:
            completed_challenge_BD=None        

        

        if challenge.tipe=='F': #la primera vez que viene --> Tiene almenos un payload, solo tiene 1 canjeo
            if completed_challenge_BD==None:
                payloads= Payload.objects.filter(client_id=userId, is_scanned=True).select_related('employee').select_related('employee__location').filter(employee__location__id=challenge.location_id).order_by('-scanned_moment')
                
                if payloads:
                    comPletedesCahllengeFirst = actualiza_completed_challenge(client,challenge,completed_challenge_BD)
                    serializer = completed_challengeSerializer(comPletedesCahllengeFirst, many=False)
                    return Response(serializer.data)
                else:
                    msg = 'error_challenge_no_requirements'
                    return Response(msg,status=status.HTTP_400_BAD_REQUEST)
                    

            else:
                msg = 'error_challenge_already_completed'
                return Response(msg,status=status.HTTP_400_BAD_REQUEST)

        elif challenge.tipe=='C': #haz X consumiciones/canjeos entre el star y el expiration date
            payloads= Payload.objects.filter(client_id=userId, is_scanned=True).select_related('employee').select_related('employee__location').filter(employee__location__id=challenge.location_id).order_by('-scanned_moment')
            
            exchanges= Exchange.objects.filter(client_id=userId, is_scanned=True).select_related('employee').select_related('employee__location').filter(employee__location__id=challenge.location_id).order_by('-scanned_moment')
            
            dates=[]

            if completed_challenge_BD==None:

                for payload in payloads:
                    scanedMoment = payload.scanned_moment.date()
                    despuesStarDate=challenge.start_date<=scanedMoment
                    antesOExpirationDate=challenge.expiration_date >= scanedMoment or (payload.scanned_moment.year==challenge.expiration_date.year and payload.scanned_moment.day==challenge.expiration_date.day and payload.scanned_moment.month==challenge.expiration_date.month)
                    if despuesStarDate and antesOExpirationDate:
                        dates.append(payload.scanned_moment)
                    else:
                        break

                for exchange in exchanges:
                    scanedMoment = exchange.scanned_moment.date()
                    despuesStarDate=challenge.start_date<=scanedMoment
                    antesOExpirationDate=challenge.expiration_date >= scanedMoment or (exchange.scanned_moment.year==challenge.expiration_date.year and exchange.scanned_moment.day==challenge.expiration_date.day and exchange.scanned_moment.month==challenge.expiration_date.month)
                    if despuesStarDate and antesOExpirationDate:
                        dates.append(exchange.scanned_moment)
                    else:
                        break          
            else:
                for payload in payloads:
                    scanedMoment = payload.scanned_moment.date()
                    scanedMomentTime=payload.scanned_moment
                    despuesStarDate=challenge.start_date<=scanedMoment
                    antesOExpirationDate=challenge.expiration_date >= scanedMoment or (payload.scanned_moment.year==challenge.expiration_date.year and payload.scanned_moment.day==challenge.expiration_date.day and payload.scanned_moment.month==challenge.expiration_date.month)
                    noCanjeado=completed_challenge_BD.last_challenge_moment<scanedMomentTime
                    if despuesStarDate and antesOExpirationDate and noCanjeado:
                        dates.append(payload.scanned_moment)
                    else:
                        break

                for exchange in exchanges:
                    scanedMoment = exchange.scanned_moment.date()
                    scanedMomentTime=payload.scanned_moment
                    despuesStarDate=challenge.start_date<=scanedMoment
                    antesOExpirationDate=challenge.expiration_date >= scanedMoment or (exchange.scanned_moment.year==challenge.expiration_date.year and exchange.scanned_moment.day==challenge.expiration_date.day and exchange.scanned_moment.month==challenge.expiration_date.month)
                    noCanjeado=completed_challenge_BD.last_challenge_moment<scanedMomentTime
                    if despuesStarDate and antesOExpirationDate and noCanjeado:
                        dates.append(exchange.scanned_moment)
                    else:
                        break      
               
            if len(dates)>= challenge.number:
                comPletedesCahllengeConsumption = actualiza_completed_challenge(client,challenge,completed_challenge_BD)
                serializer = completed_challengeSerializer(comPletedesCahllengeConsumption, many=False)
                return Response(serializer.data)
            else:
                msg = 'error_challenge_no_requirements'
                return Response(msg,status=status.HTTP_400_BAD_REQUEST)

                       

        elif challenge.tipe=='D':  #visita el bar X dias seguidos entre stardate y enddate consumiendo (gasta puntos o gana)
            payloads= Payload.objects.filter(client_id=userId, is_scanned=True).select_related('employee').select_related('employee__location').filter(employee__location__id=challenge.location_id).order_by('-scanned_moment')
            exchanges= Exchange.objects.filter(client_id=userId, is_scanned=True).select_related('employee').select_related('employee__location').filter(employee__location__id=challenge.location_id).order_by('-scanned_moment')
            dates=[]

            if completed_challenge_BD==None:

                for payload in payloads:
                    scanedMoment = payload.scanned_moment.date()
                    despuesStarDate=challenge.start_date<=scanedMoment
                    antesOExpirationDate=challenge.expiration_date >= scanedMoment or (payload.scanned_moment.year==challenge.expiration_date.year and payload.scanned_moment.day==challenge.expiration_date.day and payload.scanned_moment.month==challenge.expiration_date.month)
                    if despuesStarDate and antesOExpirationDate:
                        dates.append(payload.scanned_moment)
                    else:
                        break

                for exchange in exchanges:
                    scanedMoment = exchange.scanned_moment.date()
                    despuesStarDate=challenge.start_date<=scanedMoment
                    antesOExpirationDate=challenge.expiration_date >= scanedMoment or (exchange.scanned_moment.year==challenge.expiration_date.year and exchange.scanned_moment.day==challenge.expiration_date.day and exchange.scanned_moment.month==challenge.expiration_date.month)
                    if despuesStarDate and antesOExpirationDate:
                        dates.append(exchange.scanned_moment)
                    else:
                        break

                dates.sort() 
                datesOne=[]
                i=0
                for sorteddate in dates:
                    dateActual=dates[i]
                    if len(dates)==1:
                        datesOne.append(dateActual)
                        break
                    elif  dates[len(dates)-1]==dateActual:
                        if datesOne[len(datesOne)-1]==dateActual:
                            break
                        else:
                            datesOne.append(dateActual)
                            break
                    else:
                        datesiguiente=dates[i+1]
                        dateActualDate=dateActual.date()
                        datesiguienteDate=datesiguiente.date()
                        sonMismoDia=days_between(dateActualDate, datesiguienteDate)==0
                        if not(sonMismoDia):
                            datesOne.append(dateActual)
                        else:
                            try:
                                datesOne.remove(dateActual)
                            except:
                                pass     
                            datesOne.append(datesiguiente)

                    i=i+1
                
                validDates=[]
                i=0
                for sorteddate in datesOne:
                    dateActual=datesOne[i]
                    if len(datesOne)==1:
                        validDates.append(dateActual)
                        break
                    elif  datesOne[len(datesOne)-1]==dateActual:
                        validDates.append(dateActual)
                        break
                    else:
                        datesiguiente=datesOne[i+1]
                        sonConsecutivas=days_between(dateActual, datesiguiente)==1
                        if sonConsecutivas:
                            validDates.append(dateActual)
                    i=i+1

            else:
                for payload in payloads:
                    scanedMoment = payload.scanned_moment.date()
                    scanedMomentTime=payload.scanned_moment
                    despuesStarDate=challenge.start_date<=scanedMoment
                    antesOExpirationDate=challenge.expiration_date >= scanedMoment or (payload.scanned_moment.year==challenge.expiration_date.year and payload.scanned_moment.day==challenge.expiration_date.day and payload.scanned_moment.month==challenge.expiration_date.month)
                    noCanjeado=completed_challenge_BD.last_challenge_moment<scanedMomentTime
                    if despuesStarDate and antesOExpirationDate and noCanjeado:
                        dates.append(payload.scanned_moment)
                    else:
                        break

                for exchange in exchanges:
                    scanedMoment = exchange.scanned_moment.date()
                    scanedMomentTime=payload.scanned_moment
                    despuesStarDate=challenge.start_date<=scanedMoment
                    antesOExpirationDate=challenge.expiration_date >= scanedMoment or (exchange.scanned_moment.year==challenge.expiration_date.year and exchange.scanned_moment.day==challenge.expiration_date.day and exchange.scanned_moment.month==challenge.expiration_date.month)
                    noCanjeado=completed_challenge_BD.last_challenge_moment<scanedMomentTime
                    if despuesStarDate and antesOExpirationDate and noCanjeado:
                        dates.append(exchange.scanned_moment)
                    else:
                        break

                datesOne=[]
                i=0
                for sorteddate in dates:
                    dateActual=dates[i]
                    if len(dates)==1:
                        datesOne.append(dateActual)
                        break
                    elif  dates[len(dates)-1]==dateActual:
                        if datesOne[len(datesOne)-1]==dateActual:
                            break
                        else:
                            datesOne.append(dateActual)
                            break
                    else:
                        datesiguiente=dates[i+1]
                        dateActualDate=dateActual.date()
                        datesiguienteDate=datesiguiente.date()
                        sonMismoDia=days_between(dateActualDate, datesiguienteDate)==0
                        if not(sonMismoDia):
                            datesOne.append(dateActual)
                        else:
                            try:
                                datesOne.remove(dateActual)
                            except:
                                pass     
                            datesOne.append(datesiguiente)

                    i=i+1
                
                validDates=[]
                i=0
                for sorteddate in datesOne:
                    dateActual=datesOne[i]
                    if len(datesOne)==1:
                        validDates.append(dateActual)
                        break
                    elif  datesOne[len(datesOne)-1]==dateActual:
                        validDates.append(dateActual)
                        break
                    else:
                        datesiguiente=datesOne[i+1]
                        sonConsecutivas=days_between(dateActual, datesiguiente)==1
                        if sonConsecutivas:
                            validDates.append(dateActual)
                    i=i+1

            if len(validDates)>= challenge.number:
                comPletedesCahllengeDays = actualiza_completed_challenge(client,challenge,completed_challenge_BD)
                serializer = completed_challengeSerializer(comPletedesCahllengeDays, many=False)
                return Response(serializer.data)
            else:
                msg = 'error_challenge_no_requirements'
                return Response(msg,status=status.HTTP_400_BAD_REQUEST)

        elif challenge.tipe=='AM':#gasta X dinero en el bar entre el star y el expiration date
            payloads= Payload.objects.filter(client_id=userId, is_scanned=True).select_related('employee').select_related('employee__location').filter(employee__location__id=challenge.location_id).order_by('-scanned_moment')
            
            if completed_challenge_BD==None:
                spendMoney=0
                for payload in payloads:
                    scanedMoment = payload.scanned_moment.date()
                    despuesStarDate=challenge.start_date<=scanedMoment
                    antesOExpirationDate=challenge.expiration_date >= scanedMoment or (payload.scanned_moment.year==challenge.expiration_date.year and payload.scanned_moment.day==challenge.expiration_date.day and payload.scanned_moment.month==challenge.expiration_date.month)
                    if despuesStarDate and antesOExpirationDate:
                        spendMoney=spendMoney+payload.money
                    else:
                        break
            else:
                spendMoney=0
                for payload in payloads:
                    scanedMoment = payload.scanned_moment.date()
                    scanedMomentTime=payload.scanned_moment
                    despuesStarDate=challenge.start_date<=scanedMoment
                    antesOExpirationDate=challenge.expiration_date >= scanedMoment or (payload.scanned_moment.year==challenge.expiration_date.year and payload.scanned_moment.day==challenge.expiration_date.day and payload.scanned_moment.month==challenge.expiration_date.month)
                    noCanjeado=completed_challenge_BD.last_challenge_moment<scanedMomentTime
                    if despuesStarDate and antesOExpirationDate and noCanjeado:
                        spendMoney=spendMoney+payload.money
                    else:
                        break 

            if spendMoney>= challenge.money_spent:
                comPletedesCahllengeAcomulated = actualiza_completed_challenge(client,challenge,completed_challenge_BD)
                serializer = completed_challengeSerializer(comPletedesCahllengeAcomulated, many=False)
                return Response(serializer.data)
            else:
                msg = 'error_challenge_no_requirements'
                return Response(msg,status=status.HTTP_400_BAD_REQUEST)               

        elif challenge.tipe=='IM':#gasta X dinero en el bar hoy
            payloadsToday= Payload.objects.filter(client_id=userId, is_scanned=True).select_related('employee').select_related('employee__location').filter(employee__location__id=challenge.location_id, scanned_moment__year=today.year,scanned_moment__month=today.month,scanned_moment__day=today.day)
            if completed_challenge_BD==None:
                spendMoney=0
                for payload in payloadsToday:
                    scanedMoment = payload.scanned_moment.date()
                    despuesStarDate=challenge.start_date<=scanedMoment
                    antesOExpirationDate=challenge.expiration_date >= scanedMoment or (payload.scanned_moment.year==challenge.expiration_date.year and payload.scanned_moment.day==challenge.expiration_date.day and payload.scanned_moment.month==challenge.expiration_date.month)
                    if despuesStarDate and antesOExpirationDate:
                        spendMoney=spendMoney+payload.money
                    else:
                        break
            else:
                spendMoney=0
                for payload in payloadsToday:
                    scanedMoment = payload.scanned_moment.date()
                    scanedMomentTime=payload.scanned_moment
                    despuesStarDate=challenge.start_date<=scanedMoment
                    antesOExpirationDate=challenge.expiration_date >= scanedMoment or (payload.scanned_moment.year==challenge.expiration_date.year and payload.scanned_moment.day==challenge.expiration_date.day and payload.scanned_moment.month==challenge.expiration_date.month)
                    noCanjeado=completed_challenge_BD.last_challenge_moment<scanedMomentTime
                    if despuesStarDate and antesOExpirationDate and noCanjeado :
                        spendMoney=spendMoney+payload.money
                    else:
                        break 

            if spendMoney>= challenge.money_spent:
                comPletedesCahllengeInmediately = actualiza_completed_challenge(client,challenge,completed_challenge_BD)
                serializer = completed_challengeSerializer(comPletedesCahllengeInmediately, many=False)
                return Response(serializer.data)
            else:
                msg = 'error_challenge_no_requirements'
                return Response(msg,status=status.HTTP_400_BAD_REQUEST)
            


        elif challenge.tipe=='CM':#gasta X dinero en el bar cada día hasta llegar a X dias requeridos
            payloads= Payload.objects.filter(client_id=userId, is_scanned=True).select_related('employee').select_related('employee__location').filter(employee__location__id=challenge.location_id).order_by('-scanned_moment')
            dates=[]

            if completed_challenge_BD==None:

                for payload in payloads:
                    scanedMoment = payload.scanned_moment.date()
                    despuesStarDate=challenge.start_date<=scanedMoment
                    antesOExpirationDate=challenge.expiration_date >= scanedMoment or (payload.scanned_moment.year==challenge.expiration_date.year and payload.scanned_moment.day==challenge.expiration_date.day and payload.scanned_moment.month==challenge.expiration_date.month)
                    if despuesStarDate and antesOExpirationDate:
                        dates.append(payload.scanned_moment)
                    else:
                        break

                dates.sort() 
                datesOne=[]
                i=0
                for sorteddate in dates:
                    dateActual=dates[i]
                    if len(dates)==1:
                        datesOne.append(dateActual)
                        break
                    elif  dates[len(dates)-1]==dateActual:
                        if datesOne[len(datesOne)-1]==dateActual:
                            break
                        else:
                            datesOne.append(dateActual)
                            break
                    else:
                        datesiguiente=dates[i+1]
                        dateActualDate=dateActual.date()
                        datesiguienteDate=datesiguiente.date()
                        sonMismoDia=days_between(dateActualDate, datesiguienteDate)==0
                        if not(sonMismoDia):
                            datesOne.append(dateActual)
                        else:
                            try:
                                datesOne.remove(dateActual)
                            except:
                                pass     
                            datesOne.append(datesiguiente)

                    i=i+1
                
                validDates=[]
                i=0
                for sorteddate in datesOne:
                    dateActual=datesOne[i]
                    if len(datesOne)==1:
                        validDates.append(dateActual)
                        break
                    elif  datesOne[len(datesOne)-1]==dateActual:
                        validDates.append(dateActual)
                        break
                    else:
                        datesiguiente=datesOne[i+1]
                        sonConsecutivas=days_between(dateActual, datesiguiente)==1
                        if sonConsecutivas:
                            validDates.append(dateActual)
                    i=i+1
                if len(validDates)< challenge.number:
                    msg = 'error_challenge_no_requirements'
                    return Response(msg,status=status.HTTP_400_BAD_REQUEST)

                total=0
                for validDate in validDates:
                    payloadsValidDate= Payload.objects.filter(client_id=userId, is_scanned=True).select_related('employee').select_related('employee__location').filter(employee__location__id=challenge.location_id, scanned_moment__year=validDate.year,scanned_moment__month=validDate.month,scanned_moment__day=validDate.day)
                    spendMoney=0
                    for payloadValidDate in payloadsValidDate:
                        spendMoney=spendMoney+payloadValidDate.money
                    if spendMoney< challenge.money_spent:
                        msg = 'error_challenge_no_requirements'
                        return Response(msg,status=status.HTTP_400_BAD_REQUEST)

                    else:
                        total=total+1
                           
            else:
                for payload in payloads:
                    scanedMoment = payload.scanned_moment.date()
                    scanedMomentTime=payload.scanned_moment
                    despuesStarDate=challenge.start_date<=scanedMoment
                    antesOExpirationDate=challenge.expiration_date >= scanedMoment or (payload.scanned_moment.year==challenge.expiration_date.year and payload.scanned_moment.day==challenge.expiration_date.day and payload.scanned_moment.month==challenge.expiration_date.month)
                    noCanjeado=completed_challenge_BD.last_challenge_moment<scanedMomentTime
                    if despuesStarDate and antesOExpirationDate and noCanjeado:
                        dates.append(payload.scanned_moment)
                    else:
                        break

                dates.sort() 
                datesOne=[]
                i=0
                for sorteddate in dates:
                    dateActual=dates[i]
                    if len(dates)==1:
                        datesOne.append(dateActual)
                        break
                    elif  dates[len(dates)-1]==dateActual:
                        if datesOne[len(datesOne)-1]==dateActual:
                            break
                        else:
                            datesOne.append(dateActual)
                            break
                    else:
                        datesiguiente=dates[i+1]
                        dateActualDate=dateActual.date()
                        datesiguienteDate=datesiguiente.date()
                        sonMismoDia=days_between(dateActualDate, datesiguienteDate)==0
                        if not(sonMismoDia):
                            datesOne.append(dateActual)
                        else:
                            try:
                                datesOne.remove(dateActual)
                            except:
                                pass     
                            datesOne.append(datesiguiente)

                    i=i+1
                
                validDates=[]
                i=0
                for sorteddate in datesOne:
                    dateActual=datesOne[i]
                    if len(datesOne)==1:
                        validDates.append(dateActual)
                        break
                    elif  datesOne[len(datesOne)-1]==dateActual:
                        validDates.append(dateActual)
                        break
                    else:
                        datesiguiente=datesOne[i+1]
                        sonConsecutivas=days_between(dateActual, datesiguiente)==1
                        if sonConsecutivas:
                            validDates.append(dateActual)
                    i=i+1
                if len(validDates)< challenge.number:
                    msg = 'error_challenge_no_requirements'
                    return Response(msg,status=status.HTTP_400_BAD_REQUEST)
                total=0
                for validDate in validDates:
                    payloadsValidDate= Payload.objects.filter(client_id=userId, is_scanned=True).select_related('employee').select_related('employee__location').filter(employee__location__id=challenge.location_id, scanned_moment__year=validDate.year,scanned_moment__month=validDate.month,scanned_moment__day=validDate.day)
                    spendMoney=0
                    for payloadValidDate in payloadsValidDate:
                        spendMoney=spendMoney+payloadValidDate.money
                    if spendMoney< challenge.money_spent:
                        msg = 'error_challenge_no_requirements'
                        return Response(msg,status=status.HTTP_400_BAD_REQUEST) 
                    else:
                        total=total+1
            if total>= challenge.number:
                comPletedesCahllengeConsecutive = actualiza_completed_challenge(client,challenge,completed_challenge_BD)
                serializer = completed_challengeSerializer(comPletedesCahllengeConsecutive, many=False)
                return Response(serializer.data)
            else:   
                msg = 'error_challenge_no_requirements'
                return Response(msg,status=status.HTTP_400_BAD_REQUEST)
        else:
            msg = 'error_challenge_invalid _challenge'
            return Response(msg,status=status.HTTP_400_BAD_REQUEST)

class SmallPagesPagination(PageNumberPagination):  
    page_size = 6
class ListPaginatedChallengesAPIView(generics.ListAPIView):
    """
    List active challenges
    """
    pagination_class = SmallPagesPagination
    serializer_class = ChallengeSerializer

    lookup_url_kwarg = "id"

    def get_queryset(self):
        locationId = self.kwargs.get(self.lookup_url_kwarg)
        today=date.today()
        date_str = "9999-12-31"
        enddate = datetime.strptime(date_str, "%Y-%m-%d").date()  
        date_strOld = "1900-01-11"
        oldDate = datetime.strptime(date_strOld, "%Y-%m-%d").date()
        queryset = Challenge.objects.filter(location_id = locationId,is_active=True,start_date__range=[oldDate,today] ,expiration_date__range=[today,enddate])
        return queryset

class ListPaginatedFutureChallengesAPIView(generics.ListAPIView):
    """
    List active challenges
    """
    pagination_class = SmallPagesPagination
    serializer_class = ChallengeSerializer

    lookup_url_kwarg = "id"

    def get_queryset(self):
        locationId = self.kwargs.get(self.lookup_url_kwarg)
        try:
            accountId=self.request.user.id
            bar=Bar.objects.get(user_account=accountId)
            locationBar= Location.objects.get(pk=locationId,is_active=True)
            if not(locationBar.bar.id==bar.id):
                raise ValidationError('You can not see these futures challenges')
            today=date.today()
            mañana=today+timedelta(days=1)
            date_str = "9999-12-31"
            enddate = datetime.strptime(date_str, "%Y-%m-%d").date()
            queryset = Challenge.objects.filter(location_id = locationId,is_active=True,start_date__range=[mañana,enddate])
        except:
            raise ValidationError('You can not see these futures challenges')
        return queryset

class ListPaginatedHistoricalChallengesAPIView(generics.ListAPIView):
    """
    List active challenges
    """
    pagination_class = SmallPagesPagination
    serializer_class = ChallengeSerializer

    lookup_url_kwarg = "id"

    def get_queryset(self):
        locationId = self.kwargs.get(self.lookup_url_kwarg)
        try:
            accountId=self.request.user.id
            bar=Bar.objects.get(user_account=accountId)
            locationBar= Location.objects.get(pk=locationId,is_active=True)
            if not(locationBar.bar.id==bar.id):
                raise ValidationError('You can not see this historical')
            today=date.today()
            ayer=today-timedelta(days=1)
            date_str = "1900-01-11"
            oldDate = datetime.strptime(date_str, "%Y-%m-%d").date()
            queryset = Challenge.objects.filter(location_id = locationId,is_active=True,expiration_date__range=[oldDate,ayer])
        except:
            raise ValidationError('You can not see this historical')
        return queryset

       



                


