from django.db import models
from django.utils import timezone
from location.models import Location
from subscription.models import Subscription
from django.core.exceptions import ValidationError
from datetime import date






class Challenge(models.Model):

    TIPE_OPTIONS = (
        ('F', 'First'), #la primera vez que viene --> Tiene almenos un payload
        ('C','Consumption'),#haz X consumiciones/canjeos entre el star y el expiration date
        ('D', 'Days'), #visita el bar X dias seguidos entre stardate y enddate y consume (gasta puntos o gana)
        ('AM', 'Acomulated_money'), #gasta X dinero en el bar entre el star y el expiration date
        ('IM', 'Instantaneous_money'), #gasta X dinero en el bar hoy
        ('CM', 'consecutive_money') #gasta X dinero en el bar cada día hasta llegar a X dias requeridos
        
    )


    title = models.CharField(('Title'),max_length=50,blank=False, null=False)
    description = models.TextField(('Description'),blank=True, null=True)
    number_points = models.PositiveIntegerField(('Number of points'),blank=False, null=False)
    # photo = models.TextField(blank=True, null=True)
    photo = models.URLField(max_length = 2048, blank='true',null='true')
    is_active= models.BooleanField(('Active'),blank=False, null=False,default=True)
    start_date = models.DateField(('Start Date'),blank=False, null=False)
    expiration_date = models.DateField(('Expiration Date'),blank=False, null=False)
    tipe = models.CharField(('Type'),max_length=2,choices=TIPE_OPTIONS, null=False, blank=False)
    money_spent = models.PositiveIntegerField(('Money spent'),blank=True, null=True)
    number = models.PositiveIntegerField(('Number of days or Consumption'),blank=True, null=True)
    location = models.ForeignKey(Location, related_name='locationChallenge', on_delete=models.CASCADE)

    def __str__(self):
       return self.title

    def clean(self):   

        if(self.start_date==None) or (self.expiration_date==None):
            raise ValidationError('Start date and expiratin date can not be none')

        if self.start_date>self.expiration_date:
            raise ValidationError('Start date must be before of expiration date')
        today=date.today()
        if self.start_date<today:
            raise ValidationError('Start date must be before of today')

        if self.is_active:
            try:
                subscription = Subscription.objects.get(location_id=self.location.id, is_active=True)
            except:
                raise ValidationError('You must have a active subscription in the current location')

        if self.tipe=='F':
            if (not(self.money_spent == None or  self.money_spent =='')) or (not(self.number == None or  self.number =='')):
                raise ValidationError('If you select the type fisrt, the field money spend and number must be none')    
        elif self.tipe=='C' or self.tipe=='D':
            if self.number == None or  self.number =='' or (not(self.money_spent == None or  self.money_spent =='')):
                raise ValidationError('If you select the type days or consumption, the field number is mandatory and the fiel money spend must be none')    
        elif self.tipe=='IM' or self.tipe=='AM':
            if (self.money_spent == None or  self.money_spent =='') or (not(self.number == None or  self.number =='')):
                raise ValidationError('If you select the type instantaneous money or acomulated money, the field money spend is mandatory and the field number mut be none')
        elif self.tipe=='CM':
            if (self.money_spent == None or  self.money_spent =='') or (self.number == None or  self.number ==''):
                raise ValidationError('If you select the type consecutive_money, the field money spent and number are mandatory')        
            subscription = Subscription.objects.get(location_id=self.location.id, is_active=True)
            if not(subscription.membership.membership_type=='VIP'):
                raise ValidationError('If you select the type consecutive_money, you need a VIP subscription')

        else:
            raise ValidationError('The type is not valid')





    
    
       
       