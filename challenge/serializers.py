from rest_framework import serializers
from .models import Challenge
from location.models import Location
from location.serializers import LocationSerializer







class ChallengeSerializer(serializers.HyperlinkedModelSerializer):

    location = LocationSerializer(many=False)

    class Meta:
        model = Challenge
        fields = ('id','title', 'description', 'number_points', 'photo', 
                    'is_active','start_date','expiration_date',
                    'tipe','money_spent','number','location')
        lookup_field = 'id'            



