from django.contrib import admin
from .models import Bar

# Register your models here.

# class QuestionOptionInline(admin.TabularInline):
#     model = QuestionOption

class BarAdmin(admin.ModelAdmin):
    list_display = ('name', 'get_user_account', 'inscriptionDate')
    ordering = ('inscriptionDate',)
    search_fields = ('name','user_account__username')
    list_per_page = 10
    list_filter = ('is_banned',)
    
    def get_user_account(self, obj):
        return obj.user_account

    get_user_account.short_description = 'User name'
    get_user_account.admin_order_field = 'bar__user_account'

admin.site.register(Bar, BarAdmin)