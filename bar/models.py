from django.db import models
from phone_field import PhoneField
from actor.models import Actor
from django.core.exceptions import ValidationError
import re

# Create your models here.
class Bar(Actor):
    phone = models.TextField()

    def __str__(self):
       return self.user_account.username

    def clean(self):
        if re.search('^([0-9]{9})$|((\+)?[0-9]{11})$|((\()?[0-9]{3}(\))? [0-9]{3}-[0-9]{4})$', self.phone) is None:
            raise ValidationError('The phone number does not seem to match the pattern')

    class Meta:
        proxy: True