from django.test import TestCase


from django.test import TestCase
from django.urls import include, path, reverse
from rest_framework.test import APITestCase, URLPatternsTestCase
from rest_framework import status
from users.models import UserAccount
from bar.models import Bar
from employee.models import Employee
from location.models import Location
from membership.models import Membership
from subscription.models import Subscription
from sale.models import Sale
from system_configuration.models import System_configuration
from rest_framework.test import APIClient
from users.authorities import Authority
from datetime import datetime
from datetime import date



class API_bar_test(APITestCase, URLPatternsTestCase):
    urlpatterns = [path('', include('pointbar.urls')),]

    def setUp(self):
        super().setUp()

        userBar = UserAccount.objects.create_user('JuanBar', 'Ejemplo1', 'ammquintana77@gmail.com', Authority.BAR.value)
        userBar.is_active=True
        userBar.save()
        userSystem = UserAccount.objects.create_user('system', 'Ejemplo1', '7@gmail.com', Authority.BAR.value)
        userSystem.save()

        format_str = '%Y-%m-%d'

        config = System_configuration(
            title_export_datas_S= "Exportaci\u00f3n de datos",
			title_export_datas_E= "Data exportation",
			body_export_datas_S= "\u00a1Hola!,\r\n\r\nSe adjunta un PDF con los datos personales que tiene usted en PointBar.\r\n\r\nUn cordial saludo del equipo de PointBar.",
			body_export_datas_E= "Hello!,\r\n\r\nIt is Attached  a PDF with the personal data that you have in PointBar.\r\n\r\nA warm greeting from the PointBar team.",
			title_segurity= "Segurity Hole!! / \u00a1Agujero de seguridad!",
			body_segurity= "Dear Sir or Madam,\r\n\r\nI am sorry to inform you that there has been a computer attack on our system and we are having problems. Please rest assured that we are working on a solution.\r\n\r\nA warm greeting from the PointBar team.\r\n\r\n/\r\n\r\nEstimado/a Se\u00f1or/a, \r\n\r\nSiento comunicarle que se ha producido un ataque inform\u00e1tico a nuestro sistema y estamos teniendo problemas. Rogamos que tengan la mayor tranquilidad posible ya que  estamos trabajando en una soluci\u00f3n.\r\n\r\nUn cordial saludo del equipo de PointBar."
        )
        config.save()

        bar = Bar(
            name = 'JuanBarPayload',
            is_banned = False,
            user_account = userBar,
            phone= '672108962')
        bar.save()

        location = Location(
                name='location_test',
                description='description_test',
                email='testSale@gmail.com',
                website='https://example.com',
                logo='https://example.com',
                latitude=3,
                longitude=3,
                street='exaple_street',
                city='exaple_city',
                state='exaple_state',
                postal_code=41089,
                country='exaple_country',
                is_active=True,
                bar=bar)
        location.save()

        membership = Membership(
                titleS='title_test',
                descriptionS='description_test',
                titleE='title_test',
                descriptionE='description_test',
                price=50,
                membership_type='VIP',
                subscriptionId=3
                )
        membership.save()

        subscription = Subscription(
                membership=membership,
                location=location,
                moment=date.today(),
                is_active=True,
                renovate=True,
                paypal_subscription_id=23432)
        subscription.save()

        start = datetime.strptime('9997-11-11', format_str)
        end = datetime.strptime('9999-11-11', format_str)
        sale = Sale(
            title = 'sale_test_exchange',
            description = 'description_test',
            number_points = '33',
            start_date = start,
            expiration_date = end,
            location = location
        )
        sale.save()

        userEmployee = UserAccount.objects.create_user('JuanEmployee', 'Ejemplo1', 'emplojuan@gmail.com', Authority.EMPLOYEE.value)
        userEmployee.is_active=True
        userEmployee.save()
        employee = Employee(
            name = 'JuanEmployeePayload',
            is_banned = False,
            user_account = userEmployee,
            is_working= True,
            location=location,
            bar_field=bar,
            control=True,
            limit_points=1000000,
            granted_points=0)
        employee.save()



    def tearDown(self):
        super().tearDown()

    def client_logged(self,json_user_data):

        url = reverse('token_obtain_pair')
        response = self.client.post(url,json_user_data, format='json')
        token=response.data['access']
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)
        return client
  
    
    def test_create_bar_account(self):

        user = UserAccount.objects.create_user('Perico', 'Ejemplo1', 'perico@gmail.com', Authority.BAR.value)

        

        bar = Bar(
            name = 'Bareto1',
            is_banned = False,
            user_account = user,
            phone= '672108962')
        bar.save()
        
        
        user_data= {
                "username": "JuanBar",
                "password": "Ejemplo1"
              }
             
        bar_created = Bar.objects.get(name='Bareto1')

        self.assertEqual('672108962',bar_created.phone)


        client= API_bar_test.client_logged(self,user_data)

        url = reverse('deleteBar')
        data = {}
        response = client.post(url,data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

   

