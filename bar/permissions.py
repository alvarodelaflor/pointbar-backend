from rest_framework import permissions

class IsAuthenticatedBar(permissions.BasePermission):

    def has_permission(self, request, view):
        if (request.user.is_authenticated and (request.method == 'POST' or request.method == 'PUT' or request.method == 'DELETE')):
            return request.user.authority == 'BAR' or request.user.is_superuser
        elif request.method == 'GET':
            return True

    # def has_object_permission(self, request, view, obj):
    #     return obj.user == request.user    


