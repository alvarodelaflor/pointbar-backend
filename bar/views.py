from rest_framework import viewsets
from employee.models import Employee
from employee.serializers import EmployeeSerializer
from .models import Bar
from .permissions import IsAuthenticatedBar
from .serializers import BarSerializer
from .serializers import BarUpdateSerializer

from rest_framework.response import Response
from rest_framework import status
from location.models import Location
from users.models import UserAccount
from subscription.models import Subscription
import random
import datetime
from rest_framework.decorators import api_view, permission_classes
from django.conf import settings
import base64
import json
import requests


class BarViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """

    queryset = Bar.objects.all().order_by("name")
    serializer_class = BarSerializer


class BarGetPersonalInfo(viewsets.ModelViewSet):
    def get_queryset(self):
        user = self.request.user
        bar = Bar.objects.get(user_account_id=user.id)
        return [bar]

    serializer_class = BarSerializer


class BarUpdateViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticatedBar]
    serializer_class = BarSerializer

    def put(self, request):
        msg = "updated"
        st = status.HTTP_200_OK

        try:
            bar = Bar.objects.get(user_account_id=request.user.id)
            bar.name = request.data.get("name")
            bar.phone = request.data.get("phone")
            bar.clean()
            bar.save()
        except:
            msg = "some_of_atributes_are_invalid"
            st = status.HTTP_422_UNPROCESSABLE_ENTITY

        return Response(msg, status=st)


class BarViewEmployees(viewsets.ModelViewSet):
    def get_queryset(self):
        user = self.request.user

        bar = Bar.objects.get(user_account_id=user.id)

        return Employee.objects.filter(bar_field=bar.id).order_by("name")

    serializer_class = EmployeeSerializer


@api_view(["POST"])
@permission_classes((IsAuthenticatedBar,))
def deleteAnon(request):
    msg = "deleted"
    st = status.HTTP_200_OK

    try:
        user = UserAccount.objects.get(pk=request.user.id)
        bar = Bar.objects.get(user_account_id=request.user.id)
        randomUsername = "AnonymousBar" + str(random.randrange(1000))
        while UserAccount.objects.filter(username=randomUsername).exists():
            randomUsername = "AnonymousBar" + str(random.randrange(1000))
        user.username = randomUsername
        user.email = randomUsername + "@mail.com"
        user.is_active = False
        user.save()
        bar.name = randomUsername
        bar.inscriptionDate = datetime.date.today()
        bar.phone = str(random.randrange(600000000, 699999999))
        bar.is_banned = True
        # Model.clean(): you could use it to automatically provide a value for a field, or to do validation that requires access to more than a single field
        bar.clean()
        bar.save()
        employees = Employee.objects.filter(bar_field__pk=bar.id)
        locations = Location.objects.filter(bar__pk=bar.id)
        if employees:
            for emp in employees:
                user = UserAccount.objects.get(pk=emp.user_account.id)
                if user.is_active:
                    randomEmpUsername = "AnonymousEmployee" + str(random.randrange(1000))
                    while UserAccount.objects.filter(username=randomEmpUsername).exists():
                        randomEmpUsername = "AnonymousEmployee" + str(random.randrange(1000))
                    user.username = randomEmpUsername
                    user.email = randomEmpUsername + "@mail.com"
                    user.is_active = False
                    user.save()
                    emp.name = randomEmpUsername
                    emp.inscriptionDate = datetime.date.today()
                    emp.is_banned = True
                    emp.control = False
                    emp.limit_points = 0
                    emp.granted_points = 0
                    emp.is_working = False
                    emp.clean()
                    emp.save()
        if locations:
            for lc in locations:
                lc.is_active = False
                lc.save()
                if Subscription.objects.filter(location__pk=lc.id).exists():
                    s = Subscription.objects.get(location__pk=lc.id)
                    if s.is_active:
                        s.is_active = False
                        s.renovate = False
                        client_id = settings.PAYPAL_CLIENT_ID
                        secret = settings.PAYPAL_SECRET
                        token = str(client_id) + ":" + secret
                        encoded_bytes = base64.b64encode(token.encode("utf-8"))
                        encoded_token = str(encoded_bytes, "utf-8")
                        url = (
                            "https://api.sandbox.paypal.com/v1/billing/subscriptions/"
                            + s.paypal_subscription_id
                            + "/cancel"
                        )
                        data = {"reason": "Not satisfied with the service"}
                        headers = {
                            "Content-Type": "application/json",
                            "Authorization": "Basic " + encoded_token,
                        }
                        r = requests.post(url, data=json.dumps(data), headers=headers)
                        s.save()
    except:
        msg = "delete_failed"
        st = status.HTTP_400_BAD_REQUEST

    return Response(msg, status=st)