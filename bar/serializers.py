import re

from django.core.exceptions import ValidationError
from django.db import transaction
from rest_framework import serializers

from users.authorities import Authority
from users.models import UserAccount
from users.serializers import UserSerializer

from .models import Bar

from pointbar_notifications.notifications import send_notification
from pointbar_notifications.emails import email_verification

class BarSerializer(serializers.ModelSerializer):

    user_account = UserSerializer(required=True)
    terms_and_conditions = serializers.SerializerMethodField()

    def create(self, validated_data):
        with transaction.atomic():
            user_data = validated_data.pop('user_account')
            user = UserAccount.objects.create_user(user_data['username'], user_data['password1'], user_data['email'], Authority.BAR.value)
            print(validated_data)
            phone = validated_data.pop('phone')

            if re.search(r'^([0-9]{9})$|((\+)?[0-9]{11})$|((\()?[0-9]{3}(\))? [0-9]{3}-[0-9]{4})$', phone) is None:
                raise ValidationError('The phone number does not seem to match the pattern')

            if self.get_terms_and_conditions(self) != "True":
                raise ValidationError('You must accept our terms and conditions')


            bar, created = Bar.objects.update_or_create(user_account=user,
                                name=validated_data.pop('name'),
                                phone=phone)
            email_verification(user)
            send_notification(user, 'notification_register')
        return bar

    class Meta:
        model = Bar
        fields = ['id', 'user_account', 'name', 'phone', 'terms_and_conditions']

    def get_terms_and_conditions(self, obj):
        try:
            return self.context['request'].data['terms_and_conditions']
        except TypeError:
            return "terms_and_conditions does not exists"
        except:
            pass

class BarUpdateSerializer(serializers.ModelSerializer):

    # user_account_id = serializers.IntegerField(read_only=True)

    # def update(self, validated_data):
        # instance.user_account_id = user_account_id
        # instance.save()
        # return instance
        # user = self.request.user
        
        # bar = Bar.objects.get(user_account_id=user.id).update(name=validated_data.pop('name'), phone=validated_data.pop('phone'))

        # bar.name = validated_data.pop('name')
        # bar.phone = validated_data.pop('phone')
        # bar.save()

        # return bar

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.phone = validated_data.get('phone', instance.phone)
        instance.save()
        return instance

        

    class Meta:
        model = Bar
        fields = ['id', 'name', 'phone']
