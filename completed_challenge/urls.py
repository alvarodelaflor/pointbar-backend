from django.urls import include, path
from . import views




urlpatterns = [
    path('api/mycompletedchallenges', views.CompletedChallengesPrincipal.as_view(), name='mycompletedchallenges'),
    path('api/completedchallenge/<id>', views.CompletedChallengePerId.as_view(), name='completedchallenge')
]