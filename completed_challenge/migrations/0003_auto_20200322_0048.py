# Generated by Django 3.0.3 on 2020-03-22 00:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('completed_challenge', '0002_auto_20200321_1631'),
    ]

    operations = [
        migrations.AlterField(
            model_name='completed_challenge',
            name='last_challenge_moment',
            field=models.DateTimeField(verbose_name='Completed on'),
        ),
    ]
