from rest_framework import serializers
from .models import Completed_challenge
from client.models import Client
from challenge.models import Challenge
from challenge.serializers import ChallengeSerializer



class clientSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Client
        fields = ('id',)


class completed_challengeSerializer(serializers.HyperlinkedModelSerializer):

    client = clientSerializer(many=False)
    challenge = ChallengeSerializer(many=False)

    class Meta:
        model = Completed_challenge
        fields = ('id','last_challenge_moment', 'count', 'challenge', 'client')



