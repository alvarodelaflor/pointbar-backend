from .models import Completed_challenge
from client.models import Client
from rest_framework import viewsets, generics
from .serializers import completed_challengeSerializer
from client.permissions import IsAuthenticatedClient
#from django.core.exceptions import ValidationError



class CompletedChallengesPrincipal(generics.ListCreateAPIView):
    serializer_class = completed_challengeSerializer
    permission_classes = [IsAuthenticatedClient]
    def get_queryset(self):
        accountId=self.request.user.id
        client=Client.objects.get(user_account=accountId)
        userId=client.id
        queryset = Completed_challenge.objects.filter(client_id = userId)
        return queryset

class CompletedChallengePerId(generics.ListCreateAPIView):
    serializer_class = completed_challengeSerializer
    permission_classes = [IsAuthenticatedClient]
    lookup_url_kwarg = "id"

    def get_queryset(self):
        completedChallenId = self.kwargs.get(self.lookup_url_kwarg)
        completedChallenge=Completed_challenge.objects.get(pk=completedChallenId)
        accountId=self.request.user.id
        client=Client.objects.get(client_id=accountId)
        userId=client.id
        queryset=[]
        
        if completedChallenge.client_id != userId :
            raise ValidationError('Forbiden')
        else:
            queryset.append(completedChallenge)
            return queryset        


