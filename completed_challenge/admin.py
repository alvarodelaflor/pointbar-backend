from django.contrib import admin
from .models import Completed_challenge


class CompletedChallengeAdmin(admin.ModelAdmin):
    list_display = ('get_id','get_client', 'get_challenge', 'last_challenge_moment')
    ordering = ('last_challenge_moment',)
    search_fields = ('client__user_account__username','challenge__title',)
    list_per_page = 10

    def get_id(self, obj):
        return obj.id
    get_id.short_description = 'Completed challenge'
    
    def get_challenge(self, obj):
        return obj.challenge
    
    get_challenge.short_description = 'Challenge'
    get_challenge.admin_order_field = 'completed_challenge__challenge'

    def get_client(self, obj):
        return obj.client    

    get_client.short_description = 'Client'
    get_client.admin_order_field = 'completed_challenge__Client'

admin.site.register(Completed_challenge,CompletedChallengeAdmin)