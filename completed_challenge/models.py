from django.db import models
from django.utils import timezone
from challenge.models import Challenge
from client.models import Client
from django.core.exceptions import ValidationError
from datetime import date


class Completed_challenge(models.Model):


    last_challenge_moment = models.DateTimeField(('Completed on'),blank=False, null=False)
    count = models.PositiveIntegerField(('Times achieved'),blank=False, null=False, default=1)
    challenge = models.ForeignKey(Challenge, related_name='challenge_related', on_delete=models.CASCADE)
    client = models.ForeignKey(Client, related_name='client_related', on_delete=models.CASCADE)

    def __str__(self):
       return 'Client: {0}, challenge: {1}'.format(self.client, self.challenge) 

   
    class Meta:
        unique_together = (('challenge', 'client'),)
        verbose_name = 'Completed challenge'
        verbose_name_plural = 'Completed challenges' 

   