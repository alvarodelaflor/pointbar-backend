from django.db import models
from django.utils import timezone
from users.models import UserAccount


class Actor(models.Model):
    name = models.CharField(max_length=100, blank=False)
    inscriptionDate = models.DateField(default=timezone.now, editable=False)
    is_banned = models.BooleanField(default=False, editable=True)
    user_account = models.OneToOneField(UserAccount, on_delete=models.CASCADE,related_name='user_account')

    class Meta():
        abstract: True
    def __str__(self):
       return self.name    
