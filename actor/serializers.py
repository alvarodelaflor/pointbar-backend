from .models import Actor
from users.serializers import UserSerializer
from rest_framework import serializers

class ActorSerializer(serializers.HyperlinkedModelSerializer):

    class Meta():
        model = Actor
        fields = ['id', 'name']