from django.urls import include, path
from rest_framework import routers
from . import views



urlpatterns = [
    path('api/personalInfo/<id>/', views.PersonalInfo.as_view(), name='PersonalInfo')
]