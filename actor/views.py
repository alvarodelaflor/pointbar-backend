from django.shortcuts import render
from rest_framework.views import APIView
from pointbar_notifications.emails import send_mail
from rest_framework.response import Response
from rest_framework import status
from users.models import UserAccount
from client.models import Client
from employee.models import Employee
from system_configuration.models import System_configuration
from bar.models import Bar
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from reportlab.lib.utils import ImageReader
from io import BytesIO


class PersonalInfo(APIView):
    
    def get(self, request,id, format=None):
        try:
            accountId=self.request.user.id
            userAccount=UserAccount.objects.get(pk= accountId)

            buffer=BytesIO()
            p=canvas.Canvas(buffer, pagesize=A4)
            #p = canvas.Canvas("hola-mundo.pdf", pagesize=A4)
            w, h = A4
            p.setFont("Helvetica", 40)
            email=None
            if id=='1':
                p.setTitle('Datos Personales')
                p.drawString(140, h - 50, 'Datos Personales')
                text = p.beginText(50, h - 110)
                text.setFont("Helvetica", 11)
                text.setLeading(18)

                if userAccount.authority == 'BAR':
                    bar=Bar.objects.get(user_account=accountId)
                    
                    text.textLine('Usted tiene actualmente  una cuenta de bar y estos son los datos de su cuenta que están actualmente')
                    text.textLine('almacenados en Pointbar:')
                    text.textLine(' ')
                    text.textLine('* Nombre:  {0}'.format(bar.name))
                    text.textLine('* Nombre de usuario:  {0}'.format(bar.user_account.username))
                    email=bar.user_account.email
                    text.textLine('* Email:  {0}'.format(email))
                    text.textLine('* Telefono:  {0}'.format(bar.phone))
                    text.textLine('* Registrado en:  {0}'.format(bar.inscriptionDate)) 
                    p.drawText(text)
                
                elif userAccount.authority == 'EMPLOYEE':
                    employee=Employee.objects.get(user_account=accountId)

                    text.textLine('Usted tiene actualmente  una cuenta de empleado y estos son los datos de su cuenta que están actualmente')
                    text.textLine('almacenados en Pointbar:')
                    text.textLine(' ')
                    text.textLine('* Nombre:  {0}'.format(employee.name))
                    text.textLine('* Nombre de usuario:  {0}'.format(employee.user_account.username))
                    email=employee.user_account.email
                    text.textLine('* Email:  {0}'.format(email))
                    text.textLine('* Es empleado de:  {0}'.format(employee.bar_field))
                    if not employee.location==None:
                        text.textLine('* Trabaja en:  {0}'.format(employee.location))
                    text.textLine('* Registrado en:  {0}'.format(employee.inscriptionDate)) 
                    p.drawText(text)


                elif userAccount.authority == 'CLIENT':
                    client=Client.objects.get(user_account=accountId)

                    text.textLine('Usted tiene actualmente  una cuenta de cliente y estos son los datos de su cuenta que están actualmente')
                    text.textLine('almacenados en Pointbar:')
                    text.textLine(' ')
                    text.textLine('* Nombre:  {0}'.format(client.name))
                    text.textLine('* Apellidos:  {0}'.format(client.surname))
                    text.textLine('* Nombre de usuario:  {0}'.format(client.user_account.username))
                    email=client.user_account.email
                    text.textLine('* Email:  {0}'.format(email))
                    text.textLine('* Fecha de nacimiento:  {0}'.format(client.birth_date))
                    text.textLine('* Registrado en:  {0}'.format(client.inscriptionDate))
                    if not (client.photo==None or client.photo== ''):
                        text.textLine('* Foto:')
                        text.textLine(' ')
                        logo = ImageReader(client.photo)
                        p.drawImage(logo,94, h - 331, mask='auto', width=50, height=50)
                    p.drawText(text)

                else:
                    msg = 'You must be an actor of the system'
                    return Response(msg,status=status.HTTP_400_BAD_REQUEST)

                
                logo = ImageReader('https://i.ibb.co/MgPjdnC/logo.png')
                p.drawImage(logo,w-60, h - 50, mask='auto', width=50, height=50)
                p.drawString(50, 50, "Estos datos aportados siguen las directivas de la ley europea GDPR (General Data Protection Regulation)")
                p.drawString(w-13, 10, "1")

                system_configurationAll = System_configuration.objects.all()
                system_configuration=system_configurationAll[0]
                p.showPage()
                p.save() 
                pdf=buffer.getvalue()
                buffer.close()

                subject_var = system_configuration.title_export_datas_S
                body_var = system_configuration.body_export_datas_S
                pdf_var = pdf
                email_var = email
                # try:
                #     send_mail(subject=system_configuration.title_export_datas_S, body=system_configuration.body_export_datas_S, pdf=pdf, recipients=[email])
                # except:
                #     msg = 'emails_limit_exceded'
                #     return Response(msg,status=status.HTTP_409_CONFLICT)

            elif id =='2':
                p.setTitle('Personal Data')
                p.drawString(140, h - 50, 'Personal Data')
                text = p.beginText(50, h - 110)
                text.setFont("Helvetica", 11)
                text.setLeading(18)

                if userAccount.authority == 'BAR':
                    bar=Bar.objects.get(user_account=accountId)
                    
                    text.textLine('You currently have a bar account and these are your current account details')
                    text.textLine('stored in Pointbar:')
                    text.textLine(' ')
                    text.textLine('* Name:  {0}'.format(bar.name))
                    text.textLine('* User name:  {0}'.format(bar.user_account.username))
                    email=bar.user_account.email
                    text.textLine('* Email:  {0}'.format(email))
                    text.textLine('* Phone number:  {0}'.format(bar.phone))
                    text.textLine('* Registered on:  {0}'.format(bar.inscriptionDate)) 
                    p.drawText(text)
                
                elif userAccount.authority == 'EMPLOYEE':
                    employee=Employee.objects.get(user_account=accountId)

                    text.textLine('You currently have a employee account and these are your current account details')
                    text.textLine('stored in Pointbar:')
                    text.textLine(' ')
                    text.textLine('* Name:  {0}'.format(employee.name))
                    text.textLine('* User name:  {0}'.format(employee.user_account.username))
                    email=employee.user_account.email
                    text.textLine('* Email:  {0}'.format(email))
                    text.textLine('* You are an employee of:  {0}'.format(employee.bar_field))
                    if not employee.location==None:
                        text.textLine('* You work in:  {0}'.format(employee.location))
                    text.textLine('* Registered on:  {0}'.format(employee.inscriptionDate)) 
                    p.drawText(text)


                elif userAccount.authority == 'CLIENT':
                    client=Client.objects.get(user_account=accountId)

                    text.textLine('You currently have a client account and these are your current account details')
                    text.textLine('stored in Pointbar:')
                    text.textLine(' ')
                    text.textLine('* Name:  {0}'.format(client.name))
                    text.textLine('* Surname:  {0}'.format(client.surname))
                    text.textLine('* User name:  {0}'.format(client.user_account.username))
                    email=client.user_account.email
                    text.textLine('* Email:  {0}'.format(email))
                    text.textLine('* Birth date:  {0}'.format(client.birth_date))
                    text.textLine('* Registered on:  {0}'.format(client.inscriptionDate))
                    if not (client.photo==None or client.photo== ''):
                        text.textLine('* Photo:')
                        text.textLine(' ')
                        logo = ImageReader(client.photo)
                        p.drawImage(logo,94, h - 331, mask='auto', width=50, height=50)
                    p.drawText(text)

                else:
                    msg = 'You must be an actor of the system'
                    return Response(msg,status=status.HTTP_400_BAD_REQUEST)

                logo = ImageReader('https://i.ibb.co/MgPjdnC/logo.png')
                p.drawImage(logo,w-60, h - 50, mask='auto', width=50, height=50)
                p.drawString(50, 50, "These data follow the directives of the European law GDPR (General Data Protection Regulation)")
                p.drawString(w-13, 10, "1")
                
                system_configurationAll = System_configuration.objects.all()
                system_configuration=system_configurationAll[0]
                p.showPage()
                p.save() 
                pdf=buffer.getvalue()
                buffer.close()

                subject_var = system_configuration.title_export_datas_E
                body_var = system_configuration.body_export_datas_E
                pdf_var = pdf
                email_var = email
                # try:
                #     send_mail(subject=system_configuration.title_export_datas_E, body=system_configuration.body_export_datas_E, pdf=pdf, recipients=[email])
                # except:
                #     msg = 'emails_limit_exceded'
                #     return Response(msg,status=status.HTTP_409_CONFLICT)  
            else:
                msg = 'invalid_id'
                return Response(msg,status=status.HTTP_400_BAD_REQUEST)
        except:
            msg = 'export_data_failed'
            return Response(msg,status=status.HTTP_424_FAILED_DEPENDENCY)

        if (subject_var is not None) & (body_var is not None) & (pdf_var is not None) & (email_var is not None):
            try:
                send_mail(subject=subject_var, body=body_var, pdf=pdf_var, recipients=[email_var])
            except:
                msg = 'emails_limit_exceded'
                return Response(msg,status=status.HTTP_409_CONFLICT) 
        else:
            msg = 'export_data_failed'
            return Response(msg,status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        msg = 'Pdf sent to your email'
        return Response(msg,status=status.HTTP_200_OK)