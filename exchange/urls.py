from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()

urlpatterns = [
    path('api/exchange-point/', views.ExchangeAndPointView.as_view(), name='ExchangeAndPointView'),
    path('api/exchange/sale/', views.ExchangeCreateView.as_view(), name='ExchangeCreateView'),
    path('api/exchange/bar/', views.ExchangesFilerForBar.as_view(), name='ExchangesFilerForBar'),
    # path('api/payload/<int:payload_id>/', views.PayloadUpdate.as_view(), name='payload'),
    # path('api/payload_per_location/', views.PayloadsActivesPerLocation.as_view(), name='locationpayload'),
    # path('api/payload/', views.PayloadPerId.as_view(), name='payloadPerId')
]
