from django.contrib import admin
from .models import Exchange

class ExchangeAdmin(admin.ModelAdmin):
    list_display = ('get_id','client', 'employee','sale', 'create_moment','scanned_moment')
    ordering = ('create_moment','scanned_moment')
    search_fields = ('sale__title','client__user_account__username','employee__user_account__username')
    list_per_page = 10
    
    def get_id(self, obj):
        return obj.id

    get_id.short_description = 'Exchange'


admin.site.register(Exchange,ExchangeAdmin)


