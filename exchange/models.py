from django.db import models
from django.utils import timezone
from employee.models import Employee
from client.models import Client
from sale.models import Sale

class Exchange(models.Model):
    points = models.PositiveIntegerField(blank=False)
    qr_code = models.CharField(max_length=200, blank=False)
    create_moment = models.DateTimeField()
    scanned_moment = models.DateTimeField(null=True)
    is_scanned = models.BooleanField(blank=False)
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE,related_name='employee', blank=True, null=True)
    client = models.ForeignKey(Client, on_delete=models.CASCADE,related_name='client', blank=False, null=False)
    sale = models.ForeignKey(Sale, on_delete=models.CASCADE, related_name='sale',blank=False, null=False)

    def __str__(self):
       return 'Client: {0}, Employee: {1}, Sale: {2}'.format(self.client, self.employee, self.sale)