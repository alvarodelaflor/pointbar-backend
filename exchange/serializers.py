from .models import Exchange
from .models import Employee
from location.serializers import LocationSerializer
from users.serializers import UserAccountDetailsSerializer
from bar.serializers import BarSerializer
from sale.serializers import SaleSerializer
from client.serializers import ClientSerializer
from rest_framework import serializers

class employeeSerializer(serializers.HyperlinkedModelSerializer):
    location = LocationSerializer(many=False)
    bar_field = BarSerializer(many=False)
    user_account = UserAccountDetailsSerializer(many=False)

    class Meta:
        model = Employee
        fields = ('id', 'name', 'is_banned', 'inscriptionDate', 'user_account', 'is_working', 'location', 'bar_field')

class ExchangeSerializer(serializers.HyperlinkedModelSerializer):
    employee = employeeSerializer(many=False)
    client = ClientSerializer(many=False)
    sale = SaleSerializer(many=False)
    class Meta:
        model = Exchange
        fields = ['id', 'points', 'qr_code', 'create_moment', 'scanned_moment', 'is_scanned', 'employee', 'client', 'sale']
