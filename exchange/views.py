from .models import Exchange
from sale.models import Sale
from client.models import Client
from point.models import Point
from bar.models import Bar
from employee.models import Employee
from subscription.models import Subscription
from django.db.models import Q
from rest_framework import viewsets, generics, status
from .serializers import ExchangeSerializer
from sale.serializers import SaleSerializer
from client.permissions import IsAuthenticatedClient
from employee.permissions import IsAuthenticatedEmployee
from random import random
from django.core.exceptions import ValidationError
from datetime import date
import datetime
from rest_framework.pagination import PageNumberPagination


class ExchangesByNumber(generics.ListCreateAPIView):
    serializer_class = ExchangeSerializer

    def get_queryset(self):
        queryset = []
        number = self.request.GET.get('number')
        try:
            exchange = Exchange.objects.get(qr_code = number)
            queryset.append(exchange)
        except:
            msg = 'Exchange not found'  
            st = status.HTTP_422_UNPROCESSABLE_ENTITY
        return queryset

class ExchangeAndPointView(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticatedEmployee]
    serializer_class = SaleSerializer
        

    def get_queryset(self):
        queryset = []
        number = self.request.GET.get('code')
        exchange = Exchange.objects.get(qr_code = number)

        logged_account=self.request.user
        employee=Employee.objects.get(user_account=logged_account)

        exchange.is_scanned = True
        exchange.scanned_moment = datetime.datetime.now()
        exchange.employee = employee

        try:
            point=Point.objects.get(client_id=exchange.client.id, location_id = exchange.sale.location.id)
            if((point.quantity - exchange.points) >= 0):
                point.quantity=point.quantity - exchange.points
                sale=Sale.objects.get(pk=exchange.sale.id)
                queryset.append(sale)
                try:
                    subscription = Subscription.objects.get(location_id=employee.location.id, is_active=True)
                    exchange.save()
                    point.save()
                except:
                    # Aunque no tenga sentido, es util en frontend porque si queryset.lenght == 2 muestro que no hay suscription
                    queryset.append(sale)
            
        except:
            raise ValidationError('Something gone wrong')

        return queryset

class ExchangeCreateView(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticatedClient]
    serializer_class = ExchangeSerializer

    def get_queryset(self):
        queryset = []
        number = self.request.GET.get('sale_id')
        sale = Sale.objects.get(pk = number)

        if(sale.expiration_date < datetime.date.today()):
            raise ValidationError('You are trying to apply an expirated sale')


        logged_account=self.request.user
        client=Client.objects.get(user_account=logged_account)

        try:
            point=Point.objects.get(client_id=client.id, location_id = sale.location.id)

            try:
                exchange=Exchange.objects.get(client=client.id, sale = sale.id, is_scanned = False)
                queryset.append(exchange)
                return queryset
            except:
           
                exchange = Exchange(
                is_scanned = False,
                create_moment = datetime.datetime.now(),
                points = sale.number_points,
                client = client,
                qr_code = str(int(random() * 1000000000000000)),
                sale = sale
                )
                exchange.save()
    
                queryset.append(exchange)
                return queryset
        except:
            raise ValidationError('You have no points for this location yet')

class SmallPagesPagination(PageNumberPagination):  
    page_size = 10
    
class ExchangesFilerForBar(generics.ListCreateAPIView):
    serializer_class = ExchangeSerializer
    pagination_class = SmallPagesPagination

    def get_queryset(self):
        queryset = []
        location = self.request.GET.get('location')
        employee = self.request.GET.get('employee')
        client = self.request.GET.get('client')
        sale = self.request.GET.get('sale')
        create_moment_min = self.request.GET.get('create_moment_min')
        create_moment_max = self.request.GET.get('create_moment_max')
        create_moment_exact = self.request.GET.get('create_moment_exact')
        points_min = self.request.GET.get('points_min')
        points_max = self.request.GET.get('points_max')
        points_exact = self.request.GET.get('points_exact')
        gap = int(self.request.GET.get('gap'))

        if self.request.user.authority == 'BAR':
            logged_account=self.request.user
            bar=Bar.objects.get(user_account=logged_account)

            queryset = Exchange.objects.all()
            queryset = queryset.filter(sale__location__bar = bar.id, sale__location__is_active = True)

        else:
            logged_account=self.request.user
            clientLogged=Client.objects.get(user_account=logged_account)

            queryset = Exchange.objects.all()
            queryset = queryset.filter(client = clientLogged.id, sale__location__is_active = True)

        

        if not (location == "none"):
            queryset = queryset.filter(sale__location__name__iexact=location)

        if ((employee.lower() == "not scanned") or (employee.lower() == "no escaneado")):
            queryset = queryset.filter(employee__isnull = True)
        
        if not (employee == "none" or (employee.lower() == "not scanned") or (employee.lower() == "no escaneado")):
            queryset = queryset.exclude(employee__isnull = True)
            queryset = queryset.filter(Q(employee__name__iexact=employee) | Q(employee__user_account__username=employee))

        if not (client == "none"):
            queryset = queryset.filter(Q(client__name__iexact=client) | Q(client__user_account__username=client))

        if not (sale == "none"):
            queryset = queryset.filter(sale__title__iexact=sale)
        
        if not (create_moment_min == "none"):
            format_str = '%Y-%m-%d'
            create_moment_min = datetime.datetime.strptime(create_moment_min, format_str) 
            create_moment_min = create_moment_min+datetime.timedelta(hours=gap)
            date_str = "9999-12-31"
            enddate = datetime.datetime.strptime(date_str, "%Y-%m-%d").date()
            queryset = queryset.filter(create_moment__range=[create_moment_min,enddate])

        if not (create_moment_max == "none"):
            format_str = '%Y-%m-%d'
            create_moment_max = datetime.datetime.strptime(create_moment_max, format_str)
            create_moment_max = create_moment_max+datetime.timedelta(hours=gap) 
            date_str = "1001-12-31"
            startdate = datetime.datetime.strptime(date_str, "%Y-%m-%d").date()
            queryset = queryset.filter(create_moment__range=[startdate,create_moment_max])

        if not (create_moment_exact == "none"):
            format_str = '%Y-%m-%d'
            create_moment_exact = datetime.datetime.strptime(create_moment_exact, format_str) 
            create_moment_exact = create_moment_exact+datetime.timedelta(hours=gap) 
            enddate = create_moment_exact+datetime.timedelta(days=1)
            queryset = queryset.filter(create_moment__range=[create_moment_exact,enddate])

        if not ((points_min == "none") or (points_min == "null")):
            queryset = queryset.filter(sale__number_points__gte = points_min)

        if not ((points_max == "none") or (points_max == "null")):
            queryset = queryset.filter(sale__number_points__lte = points_max)

        if not ((points_exact == "none") or (points_exact == "null")):
            queryset = queryset.filter(sale__number_points = points_exact)

        return queryset