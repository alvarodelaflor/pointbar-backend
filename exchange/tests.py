from django.test import TestCase
from django.urls import include, path, reverse
from rest_framework.test import APITestCase, URLPatternsTestCase
from rest_framework import status
from users.models import UserAccount
from bar.models import Bar
from employee.models import Employee
from .models import Sale
from rest_framework.test import APIClient
from users.authorities import Authority
from location.models import Location
from membership.models import Membership
from subscription.models import Subscription
from client.models import Client
from sale.models import Sale
from .models import Exchange
from point.models import Point
from datetime import date
from datetime import datetime
import warnings



class API_exchange_test(APITestCase, URLPatternsTestCase):
    urlpatterns = [path('', include('pointbar.urls')),]
    warnings.filterwarnings("ignore")

    def setUp(self):

        super().setUp()

        userClient = UserAccount.objects.create_user('JuanClient', 'Ejemplo1', 'clientjuan@gmail.com', Authority.CLIENT.value)
        userClient.is_active=True
        userClient.save()
        format_str = '%Y-%m-%d'
        birth = datetime.strptime('2000-11-11', format_str)
        client = Client(
            name = 'JuanClientrExchange',
            is_banned = False,
            user_account = userClient,
            surname = 'joe',
            birth_date = birth.date(),
            phone='672108962')
        client.save()

        
        
        userBar = UserAccount.objects.create_user('JuanBar', 'Ejemplo1', 'barjuan@gmail.com', Authority.BAR.value)
        userBar.is_active=True
        userBar.save()
        bar = Bar(
            name = 'JuanBarExchange',
            is_banned = False,
            user_account = userBar,
            phone= '672108962')
        bar.save()

        location = Location(
                name='location_test',
                description='description_test',
                email='testSale@gmail.com',
                website='https://example.com',
                logo='https://example.com',
                latitude=3,
                longitude=3,
                street='exaple_street',
                city='exaple_city',
                state='exaple_state',
                postal_code=41089,
                country='exaple_country',
                is_active=True,
                bar=bar)
        location.save()

        membership = Membership(
                titleS='title_test',
                descriptionS='description_test',
                titleE='title_test',
                descriptionE='description_test',
                price=50,
                membership_type='VIP',
                subscriptionId=3
                )
        membership.save()

        subscription = Subscription(
                membership=membership,
                location=location,
                moment=date.today(),
                is_active=True,
                renovate=True,
                paypal_subscription_id=23432)
        subscription.save()

        start = datetime.strptime('9997-11-11', format_str)
        end = datetime.strptime('9999-11-11', format_str)
        sale = Sale(
            title = 'sale_test_exchange',
            description = 'description_test',
            number_points = '33',
            start_date = start,
            expiration_date = end,
            location = location
        )
        sale.save()


        userEmployee = UserAccount.objects.create_user('JuanEmployee', 'Ejemplo1', 'emplojuan@gmail.com', Authority.EMPLOYEE.value)
        userEmployee.is_active=True
        userEmployee.save()
        employee = Employee(
            name = 'JuanEmployeeExchange',
            is_banned = False,
            user_account = userEmployee,
            is_working= True,
            location=location,
            bar_field=bar,
            control=True,
            limit_points=1000000,
            granted_points=0)
        employee.save()

        point = Point(
            quantity = 100000,
            client = client,
            location = location
        )
        point.save()

        exchange = Exchange(
            points = 10,
            qr_code = 999,
            create_moment = datetime.now(),
            scanned_moment = None,
            is_scanned =False,
            client = client,
            sale = sale
        )
        exchange.save()



        

    def tearDown(self):
        super().tearDown()

    def client_logged(self,json_user_data):

        url = reverse('token_obtain_pair')
        response = self.client.post(url,json_user_data, format='json')
        token=response.data['access']
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)
        return client

    def test_get_by_number(self):

        client = APIClient()
        url = reverse('exchangeByNumber')

        data = {
            'number': 1
        }
        response = client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_exchange_point_view(self):

        user_data= {
                "username": "JuanEmployee",
                "password": "Ejemplo1"
              }
             
        client= API_exchange_test.client_logged(self,user_data)


        url = reverse('ExchangeAndPointView')
        data = {
            'code': 999
        }
        response = client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        exchange_traded = Exchange.objects.get(qr_code = 999)
        self.assertEqual(True, exchange_traded.is_scanned)     

    def test_create_exchange(self):

        user_data= {
                "username": "JuanClient",
                "password": "Ejemplo1"
              }
             
        client= API_exchange_test.client_logged(self,user_data)


        url = reverse('ExchangeCreateView')
        sale=Sale.objects.get(title = 'sale_test_exchange')
        data = {
            'sale_id': sale.id
        }
        response = client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        exchange_created = Exchange.objects.get(sale__title= 'sale_test_exchange')
        self.assertEqual(False, exchange_created.is_scanned)

    def test_filter_exchange(self):

        user_data= {
                "username": "JuanBar",
                "password": "Ejemplo1"
              }
             
        client= API_exchange_test.client_logged(self,user_data)


        url = reverse('ExchangesFilerForBar')
        
        data = {
            'location': 'test',
            'employee': 'test',
            'client': 'test',
            'sale': 'test',
            'create_moment_min':'2020-02-02',
            'create_moment_max':'2020-02-02',
            'create_moment_exact':'2020-02-02',
            'points_min':2,
            'points_max':10,
            'points_exact': 10,
            'gap':-2,
        }

        response = client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
           