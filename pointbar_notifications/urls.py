''' Django notification urls file '''
# -*- coding: utf-8 -*-
from distutils.version import StrictVersion  # pylint: disable=no-name-in-module,import-error

from django import get_version

from . import views

if StrictVersion(get_version()) >= StrictVersion('2.0'):
    from django.urls import re_path as pattern
else:
    from django.conf.urls import url as pattern


urlpatterns = [
    # pattern(r'^$', views.AllNotificationsList.as_view(), name='all'),
    # pattern(r'^unread/$', views.UnreadNotificationsList.as_view(), name='unread'),
    pattern(r'^mark-all-as-read/$', views.mark_all_as_read, name='mark_all_as_read'),
    pattern(r'^api/notification/mark-as-read/', views.mark_as_read, name='mark_as_read'),
    pattern(r'^mark-as-unread/(?P<slug>\d+)/$', views.mark_as_unread, name='mark_as_unread'),
    pattern(r'^api/notification/delete/', views.delete, name='delete'), #(?P<slug>\d+)/$
    pattern(r'^api/unread_count/$', views.live_unread_notification_count, name='live_unread_notification_count'),
    pattern(r'^api/all_count/$', views.live_all_notification_count, name='live_all_notification_count'),
    pattern(r'^api/unread_list/$', views.NotificationsUnreadList.as_view(), name='live_unread_notification_list'),
    pattern(r'^api/read_list/$', views.NotificationsReadList.as_view(), name='live_read_notification_list'),
    pattern(r'^api/all_list/', views.live_all_notification_list, name='live_all_notification_list'),
    pattern(r'^api/paypal/webhook/', views.hook_receiver_view, name='paypal-webhook')
]

app_name = 'pointbar_notifications'