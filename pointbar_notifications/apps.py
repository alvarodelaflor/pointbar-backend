from django.apps import AppConfig


class PointbarNotificationsConfig(AppConfig):
    name = 'pointbar_notifications'
