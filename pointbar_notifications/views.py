import json

from django.forms import model_to_dict
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, redirect
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_exempt
from notifications import settings
from notifications.models import Notification
from notifications.settings import get_config
from notifications.utils import id2slug, slug2id
from rest_framework import generics, status
from rest_framework.decorators import api_view
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from subscription.models import Subscription

from .notifications import send_notification
from .serializers import NotificationSerializer


@csrf_exempt
@api_view(["POST"])
def hook_receiver_view(request):
    data = json.loads(json.dumps(request.data))

    paypal_subscription_id = data['resource']['id']

    subscription = Subscription.objects.get(paypal_subscription_id=paypal_subscription_id)

    if subscription.is_active:
        subscription.is_active = False
        subscription.renovate = False
        subscription.save()
    
    send_notification(subscription.location.bar.user_account, 'notification_subscription_cancel', 'notification_subscription_cancel_description')

    return HttpResponse('success')


@api_view(['GET'])
def mark_all_as_read(request):
    request.user.notifications.mark_all_as_read()

    _next = request.GET.get('next')

    if _next:
        return redirect(_next)
    return redirect('notifications:unread')


@api_view(['POST'])
def mark_as_read(request, slug=None):
    notification_id = request.data['id']

    try:
        notification = get_object_or_404(
            Notification, id=notification_id)
        notification.mark_as_read()
        msg = 'success'
        st = status.HTTP_200_OK
    except:
        msg = 'failed'
        st = status.HTTP_404_NOT_FOUND

    return Response(msg, status=st)

@api_view(['GET'])
def mark_as_unread(request, slug=None):
    notification_id = slug2id(slug)

    notification = get_object_or_404(
        Notification, recipient=request.user, id=notification_id)
    notification.mark_as_unread()

    _next = request.GET.get('next')

    if _next:
        return redirect(_next)

    return Response('success', status.HTTP_200_OK)


@api_view(['POST'])
def delete(request):
    # notification_id = slug2id(slug)
    notification_id = request.data['id']

    try:
        notification = get_object_or_404(
            Notification, id=notification_id)

        if settings.get_config()['SOFT_DELETE']:
            notification.deleted = True
            notification.save()
        else:
            notification.delete()

        msg = 'success'
        st = status.HTTP_200_OK
    except:
        msg = 'failed'
        st = status.HTTP_404_NOT_FOUND

    return Response(msg, status=st)


@api_view(['GET'])
@never_cache
def live_unread_notification_count(request):
    user_is_authenticated = request.user.is_authenticated

    if not user_is_authenticated:
        data = {
            'unread_count': 0
        }
    else:
        data = {
            'unread_count': request.user.notifications.unread().count(),
        }
    return JsonResponse(data)

@api_view(['GET'])
@never_cache
def live_unread_notification_list(request):
    ''' Return a json with a unread notification list '''
    user_is_authenticated = request.user.is_authenticated

    if not user_is_authenticated:
        print("no se ha loggeado")
        data = {
            'unread_count': 0,
            'unread_list': []
        }
        return JsonResponse(data)

    default_num_to_fetch = get_config()['NUM_TO_FETCH']
    try:
        # If they don't specify, make it 5.
        num_to_fetch = request.GET.get('max', default_num_to_fetch)
        num_to_fetch = int(num_to_fetch)
        if not (1 <= num_to_fetch <= 100):
            num_to_fetch = default_num_to_fetch
    except ValueError:  # If casting to an int fails.
        num_to_fetch = default_num_to_fetch

    unread_list = []

    for notification in request.user.notifications.unread()[0:num_to_fetch]:
        struct = model_to_dict(notification)
        struct['slug'] = id2slug(notification.id)
        if notification.actor:
            struct['actor'] = str(notification.actor)
        if notification.target:
            struct['target'] = str(notification.target)
        if notification.action_object:
            struct['action_object'] = str(notification.action_object)
        if notification.data:
            struct['data'] = notification.data
        unread_list.append(struct)
        if request.GET.get('mark_as_read'):
            notification.mark_as_read()
    data = {
        'unread_count': request.user.notifications.unread().count(),
        'unread_list': unread_list
    }
    return JsonResponse(data)

class SmallPagesPagination(PageNumberPagination):  
    page_size = 3

class NotificationsUnreadList(generics.ListAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = NotificationSerializer
    pagination_class = SmallPagesPagination

    def get_queryset(self):
        user = self.request.user
        notifications = user.notifications.unread()
        return notifications

class NotificationsReadList(generics.ListAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = NotificationSerializer
    pagination_class = SmallPagesPagination

    def get_queryset(self):
        user = self.request.user
        notifications = user.notifications.read()
        return notifications

@api_view(['GET'])
@never_cache
def live_all_notification_list(request):
    ''' Return a json with a unread notification list '''
    user_is_authenticated = request.user.is_authenticated

    if not user_is_authenticated:
        data = {
            'all_count': 0,
            'all_list': []
        }
        return JsonResponse(data)

    default_num_to_fetch = get_config()['NUM_TO_FETCH']
    try:
        # If they don't specify, make it 5.
        num_to_fetch = request.GET.get('max', default_num_to_fetch)
        num_to_fetch = int(num_to_fetch)
        if not (1 <= num_to_fetch <= 100):
            num_to_fetch = default_num_to_fetch
    except ValueError:  # If casting to an int fails.
        num_to_fetch = default_num_to_fetch

    all_list = []

    for notification in request.user.notifications.all()[0:num_to_fetch]:
        struct = model_to_dict(notification)
        struct['slug'] = id2slug(notification.id)
        if notification.actor:
            struct['actor'] = str(notification.actor)
        if notification.target:
            struct['target'] = str(notification.target)
        if notification.action_object:
            struct['action_object'] = str(notification.action_object)
        if notification.data:
            struct['data'] = notification.data
        all_list.append(struct)
        if request.GET.get('mark_as_read'):
            notification.mark_as_read()
    data = {
        'all_count': request.user.notifications.count(),
        'all_list': all_list
    }
    return JsonResponse(data)

@api_view(['GET'])
def live_all_notification_count(request):
    user_is_authenticated = request.user.is_authenticated

    if not user_is_authenticated:
        data = {
            'all_count': 0
        }
    else:
        data = {
            'all_count': request.user.notifications.count(),
        }
    return JsonResponse(data)
