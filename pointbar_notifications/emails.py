from django.conf import settings
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from system_configuration.models import System_configuration
from users.models import UserAccount
from users.token import account_activation_token

from .notifications import send_notification


def send_mail(subject, body, recipients, pdf=None):
    email = EmailMessage(
        subject=subject,
        body=body + '\r\n\r\n--\r\nSupport Contact | Pointbar Inc.\r\n\r\nwww.pointbar.site',
        from_email='Pointbar Support <' + settings.EMAIL_HOST_USER + '>',
        to=recipients,
        reply_to=[settings.EMAIL_HOST_USER],
    )

    if pdf is None:
        for recipient in recipients:
            user = UserAccount.objects.get(email=recipient)
            send_notification(user, 'notification_security-breach', 'notification_security-breach_description')
    else:
        try:
            email.attach('data.pdf', pdf, 'application/pdf')
            user = UserAccount.objects.get(email=recipients[0])
            send_notification(user, 'notification_personal-info', 'notification_personal-info_description')
        except:
            pass

    email.send(fail_silently=False)

def email_verification(user):
    print("Enviando email")
    uid = urlsafe_base64_encode(force_bytes(user.pk))
    token = account_activation_token.make_token(user)
    
    system_configuration = System_configuration.objects.all()[0]
    verification_link = settings.FRONT_URL + '/activation/' + uid + '/' + token + '/'
    verification_link_html = '<a href=' + verification_link + '>' + verification_link + '</a>'

    body = system_configuration.body_verify.replace('\r\n', '<br>')

    email = EmailMultiAlternatives(
        subject=system_configuration.title_verify,
        body=body + "<br>" + verification_link_html + '<br><br>--<br>Support Contact | Pointbar Inc.<br><br>www.pointbar.site',
        from_email='Pointbar Support <' + settings.EMAIL_HOST_USER + '>',
        to=[user.email],
        reply_to=[settings.EMAIL_HOST_USER],
    )

    email.content_subtype = "html"
    email.send(fail_silently=False)
