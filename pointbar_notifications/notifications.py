from notifications.signals import notify
from users.models import UserAccount

def send_notification(recipient, title, description=None):
    system_user = UserAccount.objects.get(username='system')
    
    if description is None:
        notify.send(sender=system_user, recipient=recipient, verb=title)
    else:
        notify.send(sender=system_user, recipient=recipient, verb=title, description=description)