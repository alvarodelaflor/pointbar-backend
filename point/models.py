from django.db import models
from client.models import Client
from location.models import Location

# Create your models here.

class Point(models.Model):
    quantity = models.IntegerField(blank=False)
    client = models.ForeignKey(Client, on_delete=models.CASCADE, blank=False, null=False)
    location = models.ForeignKey(Location, on_delete=models.CASCADE, blank=False, null=False)

    def __str__(self):
       return 'Client: {0}, Location: {1}'.format(self.client, self.location)
