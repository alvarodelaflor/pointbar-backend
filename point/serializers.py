from .models import Point
from location.models import Location
from client.models import Client
from rest_framework import serializers
from location.serializers import LocationSerializer
from client.serializers import ClientSerializer


class PointSerializer(serializers.HyperlinkedModelSerializer):
   
    location = LocationSerializer(many=False)
    client = ClientSerializer(many=False)
    
    class Meta:
        model = Point
        fields = ['id', 'quantity', 'client', 'location']
