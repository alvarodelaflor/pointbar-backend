from django.shortcuts import render
from rest_framework import viewsets, generics, status
from .serializers import PointSerializer
from .models import Point
from client.models import Client
from client.permissions import IsAuthenticatedClient
from bar.permissions_superuser import IsAuthenticatedSuperUser
from django.core.exceptions import ValidationError
from rest_framework.pagination import PageNumberPagination


class PointView(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticatedSuperUser]
    queryset = Point.objects.all()
    serializer_class = PointSerializer
    
    def get(self, request, *args, **kwargs):
        self.serializer_class = PointSerializer
        return super().get(request, *args, **kwargs)

class PointByClientAndLocation(generics.ListCreateAPIView):
    serializer_class = PointSerializer
    permission_classes = [IsAuthenticatedClient]
    lookup_url_kwarg = "id"

    def get_queryset(self):
        queryset = []
        locationId = self.kwargs.get(self.lookup_url_kwarg)
        accountId=self.request.user.id
        client=Client.objects.get(user_account=accountId)
        try:
            point = Point.objects.get(client = client.id,location = locationId)
            queryset.append(point)
        except:
            msg = 'points not found'  
            st = status.HTTP_422_UNPROCESSABLE_ENTITY
        
        return queryset

class SmallPagesPagination(PageNumberPagination):
    page_size = 8


class ListPaginatedPointAPIView(generics.ListAPIView):

    pagination_class = SmallPagesPagination
    permission_classes = [IsAuthenticatedClient]
    serializer_class = PointSerializer

    def get_queryset(self):
        user = self.request.user
        if(user.is_anonymous is False and user.authority == 'CLIENT'):
            client_id = Client.objects.get(user_account=self.request.user).id
            queryset = Point.objects.filter(client=client_id, location__is_active=True)
        return queryset