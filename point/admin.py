from django.contrib import admin
from .models import Point 


class PointAdmin(admin.ModelAdmin):
    list_display = ('get_id','client', 'location','quantity')
    ordering = ('quantity',)
    search_fields = ('client__user_account__username','location__name')
    list_per_page = 10

    def get_id(self, obj):
        return obj.id

    get_id.short_description = 'Points'


admin.site.register(Point,PointAdmin)
