from django.urls import include, path
from . import views


urlpatterns = [
    path('api/points', views.PointView.as_view(), name='point'),
    path('api/point/location/<id>', views.PointByClientAndLocation.as_view(), name='pointoflocation'),
    path('api/point/pagination', views.ListPaginatedPointAPIView.as_view(), name='pagination')
]