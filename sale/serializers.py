from rest_framework import serializers
from location.serializers import LocationSerializer
from .models import Sale
from location.models import Location




class locationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Location
        fields = ('id',)


class SaleSerializer(serializers.HyperlinkedModelSerializer):

    location = LocationSerializer(many=False)

    class Meta:
        model = Sale
        fields = ('id','title', 'description', 'number_points', 'photo', 
                    'is_active','start_date','expiration_date','location')



