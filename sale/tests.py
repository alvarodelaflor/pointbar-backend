from django.test import TestCase
from django.urls import include, path, reverse
from rest_framework.test import APITestCase, URLPatternsTestCase
from rest_framework import status
from users.models import UserAccount
from bar.models import Bar
from .models import Sale
from rest_framework.test import APIClient
from users.authorities import Authority
from location.models import Location
from membership.models import Membership
from subscription.models import Subscription
from datetime import date


class API_sales_test(APITestCase, URLPatternsTestCase):
    urlpatterns = [path('', include('pointbar.urls')),]

    def setUp(self):

        super().setUp()

        user = UserAccount.objects.create_user('JuanBar', 'Ejemplo1', 'juan@gmail.com', Authority.BAR.value)
        user.is_active=True
        user.save()
        bar = Bar(
            name = 'JuanBarSales',
            is_banned = False,
            user_account = user,
            phone= '672108962')
        bar.save()

        location = Location(
                name='location_test',
                description='description_test',
                email='testSale@gmail.com',
                website='https://example.com',
                logo='https://example.com',
                latitude=3,
                longitude=3,
                street='exaple_street',
                city='exaple_city',
                state='exaple_state',
                postal_code=41089,
                country='exaple_country',
                is_active=True,
                bar=bar)
        location.save()

        membership = Membership(
                titleS='title_test',
                descriptionS='description_test',
                titleE='title_test',
                descriptionE='description_test',
                price=50,
                membership_type='VIP',
                subscriptionId=3
                )
        membership.save()

        subscription = Subscription(
                membership=membership,
                location=location,
                moment=date.today(),
                is_active=True,
                renovate=True,
                paypal_subscription_id=23432)
        subscription.save()

        

    def tearDown(self):
        super().tearDown()

    def client_logged(self,json_user_data):

        url = reverse('token_obtain_pair')
        response = self.client.post(url,json_user_data, format='json')
        token=response.data['access']
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)
        return client

    def test_get_all(self):

        client = APIClient()
        url = reverse('sales')
        response = client.get(url, data={'format': 'json'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)    
    
    def test_create_update_delete_sale(self):

        user_data= {
                "username": "JuanBar",
                "password": "Ejemplo1"
              }
             
        client= API_sales_test.client_logged(self,user_data)
        
        #create the sale
        url = reverse('sales')
        location=Location.objects.get(email='testSale@gmail.com')
        data = {
            'title': 'sale_test',
            'description': 'description_test',
            'number_points': '33',
            'start_date': '9998-02-02',
            'expiration_date' : '9999-03-03',
            'location' : {'id':location.id,}
        }

        response = client.post(url,data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        sale_saved=Sale.objects.get(title='sale_test')
        self.assertEqual(33,sale_saved.number_points)

        #update the sale

        kwargs = {
            'id': sale_saved.id,
        }
        url = reverse('sale', kwargs=kwargs)
        data = {
            'title': 'sale_UPDATED',
            'description': 'description_test',
            'number_points': '40',
            'start_date': '9998-02-02',
            'expiration_date' : '9999-03-03',
            'location' : {'id':sale_saved.location.id,}
        }

        response = client.put(url,data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        sale_updated=Sale.objects.get(title='sale_UPDATED')
        self.assertEqual(40,sale_updated.number_points)

        #delete sale

        kwargs = {
            'id': sale_saved.id,
        }
        url = reverse('sale', kwargs=kwargs)

        response = client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        sale_deleted=Sale.objects.get(title='sale_UPDATED')
        self.assertEqual(False,sale_deleted.is_active)

    def test_get_historical(self):

        user_data= {
                "username": "JuanBar",
                "password": "Ejemplo1"
              }
             
        client= API_sales_test.client_logged(self,user_data)
        location=Location.objects.get(email='testSale@gmail.com')
        kwargs = {
            'id': location.id,
        }

        url = reverse('historical_location_sale', kwargs=kwargs)
        response = client.get(url, data={'format': 'json'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)


    def test_get_future_sales(self):

        user_data= {
                "username": "JuanBar",
                "password": "Ejemplo1"
              }
             
        client= API_sales_test.client_logged(self,user_data)
        location=Location.objects.get(email='testSale@gmail.com')
        kwargs = {
            'id': location.id,
        }

        url = reverse('future_location_sale', kwargs=kwargs)
        response = client.get(url, data={'format': 'json'})
        self.assertEqual(response.status_code, status.HTTP_200_OK) 


    def test_get_now_sales(self):

        user_data= {
                "username": "JuanBar",
                "password": "Ejemplo1"
              }
             
        client= API_sales_test.client_logged(self,user_data)
        location=Location.objects.get(email='testSale@gmail.com')
        kwargs = {
            'id': location.id,
        }

        url = reverse('paginated_sales', kwargs=kwargs)
        response = client.get(url, data={'format': 'json'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)           












     


