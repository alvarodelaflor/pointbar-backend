from django.db import models
from django.utils import timezone
from location.models import Location
from subscription.models import Subscription
from django.core.exceptions import ValidationError
from datetime import date



class Sale(models.Model):
    title = models.CharField(('Title'),max_length=50,blank=False, null=False)
    description = models.TextField(('Description'),blank=True, null=True)
    number_points = models.PositiveIntegerField(('Number of points'),blank=False, null=False)
    # photo = models.TextField(blank=True, null=True)
    photo = models.URLField(max_length = 2048, blank='true',null='true')
    is_active= models.BooleanField(('Active'),blank=False, null=False,default=True)
    start_date = models.DateField(('Start Date'),blank=False, null=False)
    expiration_date = models.DateField(('Expiration Date'),blank=False, null=False)
    location = models.ForeignKey(Location, related_name='locationSale', on_delete=models.CASCADE)

    def __str__(self):
       return self.title
   
    def clean(self): 

        if(self.start_date==None) or (self.expiration_date==None):
            raise ValidationError('Start date and expiratin date can not be none')  

        if self.start_date>self.expiration_date:
            raise ValidationError('Start date must be before of expiration date')
        
        today=date.today()
        if self.start_date<today:
            raise ValidationError('Start date must be before of today')    
        
        if self.is_active:
            try:
                subscription = Subscription.objects.get(location_id=self.location.id, is_active=True)
            except:
                raise ValidationError('You must have a active subscription in the current location')
        
            
  




    
    
       
       