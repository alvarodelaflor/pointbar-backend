from django.conf import settings
from django.utils import timezone
from django.shortcuts import get_object_or_404
from rest_framework import generics, status
from rest_framework.response import Response
from .models import Sale
from bar.models import Bar
from location.models import Location
from .serializers import SaleSerializer
from bar.permissions import IsAuthenticatedBar
from rest_framework import generics, viewsets
from django.core.exceptions import ValidationError
from datetime import date
from datetime import datetime
from datetime import timedelta
from rest_framework.pagination import PageNumberPagination



class SaleView(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticatedBar]
    queryset = Sale.objects.filter(is_active=True)
    serializer_class = SaleSerializer
    
    def get(self, request, *args, **kwargs):
        self.serializer_class = SaleSerializer
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        st=status.HTTP_201_CREATED
        msg='ok, created'
        
        for data in ['title', 'description', 'number_points', 
                    'start_date','expiration_date','location']:
            if not data in request.data:
                return Response({}, status=status.HTTP_400_BAD_REQUEST)

        
        locationIn=request.data.get('location')
        location_id=locationIn['id']
        try: 
            accountId=request.user.id
            bar=Bar.objects.get(user_account=accountId)
            locationBar= Location.objects.get(pk=location_id,is_active=True)
            if not(locationBar.bar.id==bar.id):
                return Response({}, status=status.HTTP_401_UNAUTHORIZED)
        except:   
            return Response({}, status=status.HTTP_401_UNAUTHORIZED)
        
        try: 
            location = Location.objects.get(pk = location_id)  
        except:   
            return Response({}, status=status.HTTP_404_NOT_FOUND) 
        
        try: 
            format_str = '%Y-%m-%d'
            datetimeStart = datetime.strptime(request.data.get('start_date'), format_str)
            datetimeExpiration = datetime.strptime(request.data.get('expiration_date'), format_str)
            sale = Sale(
                title=request.data.get('title'), 
                description=request.data.get('description'),
                number_points=request.data.get('number_points'),
                photo=request.data.get('photo'),
                is_active=True,
                start_date= datetimeStart.date(),
                expiration_date=datetimeExpiration.date(),
                location=location)
            
            sale.clean()
            sale.save()
             
        except:
            msg = 'some of atributes are invalid'  
            st = status.HTTP_422_UNPROCESSABLE_ENTITY

        return Response(msg, status=st)


class SaleUpdate(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = SaleSerializer
    queryset = Sale.objects.all()
    lookup_field = 'id'

    def get(self, request, id, *args, **kwargs):
        self.serializer_class = SaleSerializer
        queryset = Sale.objects.get(id = id)
        today=date.today()
        if not(queryset.is_active):
            raise ValidationError('You can not see this sale')
        
        try:
            isBar=self.request.user.authority == 'BAR'
        except:
            isBar=False
        
        if (queryset.expiration_date<today) and (isBar):
            accountId=self.request.user.id
            bar=Bar.objects.get(user_account=accountId)
            if not(queryset.location.bar.id==bar.id):
                raise ValidationError('You can not see this sale')
        else: 
            if (queryset.expiration_date<today):
                raise ValidationError('You can not see this sale')
        
        return super().get(request, *args, **kwargs) 


    def put(self, request, id, *args, **kwars):   
        sale = get_object_or_404(Sale, pk=id)
        st = status.HTTP_200_OK
        msg = 'updated'

        if not(sale.is_active):
            return Response({}, status=status.HTTP_403_FORBIDDEN)


        location_id=sale.location.id

        try: 
            accountId=request.user.id
            bar=Bar.objects.get(user_account=accountId)
            locationBar= Location.objects.get(pk=location_id,is_active=True)
            if not(locationBar.bar.id==bar.id):
                return Response({}, status=status.HTTP_401_UNAUTHORIZED)
        except:   
            return Response({}, status=status.HTTP_401_UNAUTHORIZED)

        try: 
            location = Location.objects.get(pk = location_id)  
        except:
            msg = 'location not found'   
            st = status.HTTP_404_NOT_FOUND
        else:
            try:
                format_str = '%Y-%m-%d'
                datetimeStart = datetime.strptime(request.data.get('start_date'), format_str)
                datetimeExpiration = datetime.strptime(request.data.get('expiration_date'), format_str) 
                sale.title=request.data.get('title')
                sale.description= request.data.get('description')
                sale.number_points= request.data.get('number_points')
                sale.photo = request.data.get('photo')
                sale.is_active = True
                sale.start_date = datetimeStart.date()
                sale.expiration_date= datetimeExpiration.date()                    
                sale.location=location
                sale.clean()
                sale.save() 
            except: 
                msg = 'some of atributes are invalid'  
                st = status.HTTP_422_UNPROCESSABLE_ENTITY

        return Response(msg, status=st)

    def delete(self, request, id, *args, **kwars):
        sale = get_object_or_404(Sale, pk=id)
        st = status.HTTP_200_OK
        msg = 'deleted'
        if not(sale.is_active):
            return Response({}, status=status.HTTP_403_FORBIDDEN)

        location_id=sale.location.id
        
        try: 
            accountId=request.user.id
            bar=Bar.objects.get(user_account=accountId)
            locationBar= Location.objects.get(pk=location_id,is_active=True)
            if not(locationBar.bar.id==bar.id):
                return Response({}, status=status.HTTP_401_UNAUTHORIZED)
        except:
            return Response({}, status=status.HTTP_401_UNAUTHORIZED)
     
        try: 
            sale.is_active=False
            sale.save()
        except: 
            msg = 'cannot delete'  
            st = status.HTTP_409_CONFLICT

        return Response(msg, status=st)


class SmallPagesPagination(PageNumberPagination):  
    page_size = 8


                   
class HistoricalSalesPerLocation(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticatedBar]
    pagination_class = SmallPagesPagination
    serializer_class = SaleSerializer
    lookup_url_kwarg = "id"
    queryset=[]

    def get_queryset(self):
        locationId = self.kwargs.get(self.lookup_url_kwarg)
        try:
            accountId=self.request.user.id
            bar=Bar.objects.get(user_account=accountId)
            locationBar= Location.objects.get(pk=locationId,is_active=True)
            if not(locationBar.bar.id==bar.id):
                raise ValidationError('You can not see this historical')
            today=date.today()
            ayer=today-timedelta(days=1)
            date_str = "1900-01-11"
            oldDate = datetime.strptime(date_str, "%Y-%m-%d").date()
            queryset = Sale.objects.filter(location_id = locationId,is_active=True,expiration_date__range=[oldDate,ayer]).order_by('-id')
        except:
            raise ValidationError('You can not see this historical')
        return queryset 

class FutureSalesPerLocation(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticatedBar]
    pagination_class = SmallPagesPagination
    serializer_class = SaleSerializer
    lookup_url_kwarg = "id"
    queryset=[]

    def get_queryset(self):
        locationId = self.kwargs.get(self.lookup_url_kwarg)
        try:
            accountId=self.request.user.id
            bar=Bar.objects.get(user_account=accountId)
            locationBar= Location.objects.get(pk=locationId,is_active=True)
            if not(locationBar.bar.id==bar.id):
                raise ValidationError('You can not see these futures sales')
            today=date.today()
            mañana=today+timedelta(days=1)
            date_str = "9999-12-31"
            enddate = datetime.strptime(date_str, "%Y-%m-%d").date()
            queryset = Sale.objects.filter(location_id = locationId,is_active=True,start_date__range=[mañana,enddate]).order_by('-id')
        except:
            raise ValidationError('You can not see these futures sales')
        return queryset


class ListPaginatedSalesAPIView(generics.ListAPIView):
    """
    List active sales
    """
    pagination_class = SmallPagesPagination
    serializer_class = SaleSerializer

    lookup_url_kwarg = "id"
    def get_queryset(self):
        locationId = self.kwargs.get(self.lookup_url_kwarg)
        today=date.today()
        date_str = "9999-12-31"
        enddate = datetime.strptime(date_str, "%Y-%m-%d").date()
        date_strOld = "1900-01-11"
        oldDate = datetime.strptime(date_strOld, "%Y-%m-%d").date()   
        queryset = Sale.objects.filter(location_id = locationId,is_active=True,start_date__range=[oldDate,today], expiration_date__range=[today,enddate]).order_by('-id')
        return queryset

class ListPaginatedFutureSalesAPIView(generics.ListAPIView):
    """
    List active sales
    """
    pagination_class = SmallPagesPagination
    serializer_class = SaleSerializer

    lookup_url_kwarg = "id"
    def get_queryset(self):
        locationId = self.kwargs.get(self.lookup_url_kwarg)
        try:
            accountId=self.request.user.id
            bar=Bar.objects.get(user_account=accountId)
            locationBar= Location.objects.get(pk=locationId,is_active=True)
            if not(locationBar.bar.id==bar.id):
                raise ValidationError('You can not see these futures sales')
            today=date.today()
            mañana=today+timedelta(days=1)
            date_str = "9999-12-31"
            enddate = datetime.strptime(date_str, "%Y-%m-%d").date()
            queryset = Sale.objects.filter(location_id = locationId,is_active=True,start_date__range=[mañana,enddate])
        except:
            raise ValidationError('You can not see these futures sales')
        return queryset

class ListPaginatedHistoricalSalesAPIView(generics.ListAPIView):
    """
    List active sales
    """
    pagination_class = SmallPagesPagination
    serializer_class = SaleSerializer

    lookup_url_kwarg = "id"
    def get_queryset(self):
        locationId = self.kwargs.get(self.lookup_url_kwarg)
        try:
            accountId=self.request.user.id
            bar=Bar.objects.get(user_account=accountId)
            locationBar= Location.objects.get(pk=locationId,is_active=True)
            if not(locationBar.bar.id==bar.id):
                raise ValidationError('You can not see this historical')
            today=date.today()
            ayer=today-timedelta(days=1)
            date_str = "1900-01-11"
            oldDate = datetime.strptime(date_str, "%Y-%m-%d").date()
            queryset = Sale.objects.filter(location_id = locationId,is_active=True,expiration_date__range=[oldDate,ayer])
        except:
            raise ValidationError('You can not see this historical')
        return queryset