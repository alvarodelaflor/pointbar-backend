from django.contrib import admin
from .models import Sale


class SaleAdmin(admin.ModelAdmin):
    list_display = ('title', 'number_points', 'start_date','expiration_date')
    ordering = ('start_date','expiration_date')
    search_fields = ('title','number_points')
    list_filter = ('location','is_active',)
    list_per_page = 10



admin.site.register(Sale,SaleAdmin)