from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()

urlpatterns = [
    path('api/sales', views.SaleView.as_view(), name='sales'),
    path('api/sales/<id>/', views.SaleUpdate.as_view(), name='sale'),
    path('api/historicalsalesperlocation/<id>/', views.HistoricalSalesPerLocation.as_view(), name='historical_location_sale'),
    path('api/futuresales/<id>/', views.FutureSalesPerLocation.as_view(), name='future_location_sale'),
    path('api/paginatedsales/<id>/', views.ListPaginatedSalesAPIView().as_view(), name='paginated_sales'),
    path('api/paginatedfuturesales/<id>/', views.ListPaginatedFutureSalesAPIView().as_view(), name='paginated_future_sales'),
    path('api/paginatedhistoricalsales/<id>/', views.ListPaginatedHistoricalSalesAPIView().as_view(), name='paginated_historical_sales')
]