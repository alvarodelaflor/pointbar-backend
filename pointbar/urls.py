import pointbar_notifications.urls
from allauth.account.views import confirm_email
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from rest_framework_simplejwt import views as jwt_views

from bar import views as views_bar
from client import views as views_client
from employee import views as views_employee
from exchange import views as views_exchange
from payload import views as views_payload
from users import views as views_users

router = routers.DefaultRouter()
router.register(r'bar/create', views_bar.BarViewSet)
router.register(r'bar/employees', views_bar.BarViewEmployees, 'bar-employees')
router.register(r'bar/update', views_bar.BarUpdateViewSet, 'bar-update')
router.register(r'bar/get', views_bar.BarGetPersonalInfo, 'bar-get')
router.register(r'client/create', views_client.ClientViewSet)
router.register(r'client/update', views_client.ClientUpdateViewSet, 'client-update')
router.register(r'client/get', views_client.ClientGetPersonalInfo, 'client-get')
router.register(r'employee/create', views_employee.EmployeeViewSet)
router.register(r'employee/get', views_employee.EmployeeGetPersonalInfo, 'employee-get')


urlpatterns = [
    path('api/employee/user/', views_employee.EmployeeByUserAccountId.as_view()),
    path('api/client/user/', views_client.ClientByUserAccountId.as_view()),
    path('api/client/all/', views_client.getAllClient.as_view()),
    path('api/exchange/code/', views_exchange.ExchangesByNumber.as_view(), name='exchangeByNumber'),
    path('api/payload/code/', views_payload.PayloadsByNumber.as_view(), name='PayloadsByNumber'),
    path('api/bar/delete/', views_bar.deleteAnon, name='deleteBar'),
    path('api/client/delete/', views_client.deleteAnon, name='deleteClient'),
    path('api/employee/delete/', views_employee.deleteAnon, name='deleteEmployee'),
    path('api/employee/bar/delete/', views_employee.deleteBarAnon, name='deleteBarEmployee'),
    url(r'^', include('payload.urls')),
    url(r'^', include('exchange.urls')),
    path('api/', include(router.urls)),
    url(r'^', include('sale.urls')),
    url(r'^', include('location.urls')),
    url(r'^', include('subscription.urls')),
    url(r'^', include('point.urls')),
    url(r'^', include('completed_challenge.urls')),
    url(r'^', include('challenge.urls')),
    url(r'^', include('actor.urls')),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('admin/', admin.site.urls),
    url(r'^rest-auth/', include('rest_auth.urls')),
    # url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),
    url(r'^account/', include('allauth.urls')),
    url(r'^accounts-rest/registration/account-confirm-email/(?P<key>.+)/$', confirm_email, name='account_confirm_email'),
    # JWT
    path('api/token/', views_users.MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
   # path('api/location/', include('location.urls', 'location_api')),
    url(r'^', include('location.urls')),
    path('api/membership/', include('membership.urls', 'membership_api')),
    path('api/employee/', include('employee.urls', 'employee_api')),
    # JWT
    path('api/token/', views_users.MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/employee/update/<id>/', views_employee.EmployeeUpdateViewSet.as_view(), name='employee-update'),
    path('api/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    url(r'^api/account/activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views_users.activate, name='account_activate'),
    url('^', include(pointbar_notifications.urls, namespace='notifications')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

admin.site.site_header = "PointBar admin site"
admin.site.site_title = "PointBar admin site"
admin.site.index_title = "Welcome to PointBar admin site"

