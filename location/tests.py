from django.test import TestCase
from django.urls import include, path, reverse
from rest_framework.test import APITestCase, URLPatternsTestCase
from rest_framework import status
from users.models import UserAccount
from bar.models import Bar
from location.models import Location
from rest_framework.test import APIClient
from users.authorities import Authority
from location.models import Location
from membership.models import Membership
from subscription.models import Subscription
from datetime import date


class API_location_test(APITestCase, URLPatternsTestCase):
    urlpatterns = [path('', include('pointbar.urls')),]

    def setUp(self):

        super().setUp()

        user = UserAccount.objects.create_user('JuanBarLocation', 'Ejemplo1', 'juanLocation@gmail.com', Authority.BAR.value)
        user.is_active=True
        user.save()

        bar = Bar(
            name = 'JuanBarLocation',
            is_banned = False,
            user_account = user,
            phone= '672108962')
        bar.save()

        

    def tearDown(self):
        super().tearDown()

    def client_logged(self,json_user_data):

        url = reverse('token_obtain_pair')
        response = self.client.post(url,json_user_data, format='json')
        token=response.data['access']
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)
        return client

    def test_get_all(self):

        client = APIClient()
        url = reverse('location')
        response = client.get(url, data={'format': 'json'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)    
    
    def test_create_update_delete_location(self):

        user_data= {
                "username": "JuanBarLocation",
                "password": "Ejemplo1"
              }
             
        client= API_location_test.client_logged(self,user_data)
        
        #create the location
        url = reverse('location')
        bar=Bar.objects.get(name='JuanBarLocation')
        
        data = {
            'name': 'location_test',
            'description': 'description_test',
            'email': 'location@gmail.com',
            'website': 'https://unawebsite.com',
            'logo' : 'https://unawebsite.com',
            'latitude' : '9',
            'longitude' : '5',
            'street' : 'C\ Bonifacio nº3',
            'city' : 'Sevilla',
            'postal_code' : '41410',
            'photo':'{http://juan.jpg}',
            'country' : 'Spain',
            'state':'ejemplos de estado',
            'bar' : {'id':bar.id,}
        }

        response = client.post(url,data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        location_saved=Location.objects.get(name='location_test')
        self.assertEqual(9,location_saved.latitude)

        #update the location

        kwargs = {
            'id': location_saved.id,
        }
        url = reverse('edit', kwargs=kwargs)

        data = {
            'name': 'location_test_up',
            'description': 'description_test_up',
            'email': 'location@gmail.com',
            'website': 'https://unawebsite.com',
            'logo' : 'https://unawebsite.com',
            'latitude' : '9',
            'longitude' : '5',
            'street' : 'C\ Bonifacio nº3',
            'city' : 'Sevilla',
            'photo':'{http://juan.jpg}',
            'postal_code' : '41410',
            'country' : 'Spain',
            'state':'ejemplos de estado',
            'bar' : {'id':bar.id,}
        }

        response = client.put(url,data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        location_saved=Location.objects.get(country='Spain')
        self.assertEqual('description_test_up',location_saved.description)

        #delete location

        kwargs = {
            'id': location_saved.id,
        }
        url = reverse('delete', kwargs=kwargs)

        response = client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        location_deleted=Location.objects.get(country='Spain')
        self.assertEqual(False,location_deleted.is_active)


    def test_get_all_active(self):

        client = APIClient()
        url = reverse('list_active')
        response = client.get(url, data={'format': 'json'})
        self.assertEqual(response.status_code, status.HTTP_200_OK) 


    def test_get_per_bar(self):

        user_data= {
                "username": "JuanBarLocation",
                "password": "Ejemplo1"
              }
             
        client= API_location_test.client_logged(self,user_data)
        url = reverse('list_per_bar')
        response = client.get(url, data={'format': 'json'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)  


    def test_get_all_not_subscribed(self):

        user_data= {
                "username": "JuanBarLocation",
                "password": "Ejemplo1"
              }
             
        client= API_location_test.client_logged(self,user_data)
        url = reverse('list_per_bar_not_subscription')
        response = client.get(url, data={'format': 'json'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)         












     



