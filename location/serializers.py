from .models import Location
from rest_framework import serializers
from bar.models import Bar

class barSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Bar
        fields = ('id','name')


class LocationSerializer(serializers.HyperlinkedModelSerializer):
    bar = barSerializer(many=False)

    class Meta:
        model = Location
        fields = ['id',
                  'name',
                  'description',
                  'email',
                  'website',
                  'logo',
                  'photo',
                  'latitude',
                  'longitude',
                  'street',
                  'city',
                  'state',
                  'postal_code',
                  'country',
                  'is_active',
                  'bar',
                  ]


class LogicDeleteLocationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Location
        fields = ['id',
                  'name',
                  'description',
                  'email',
                  'website',
                  'logo',
                  'photo',
                  'latitude',
                  'longitude',
                  'street',
                  'city',
                  'state',
                  'postal_code',
                  'country',
                  'is_active']
        extra_kwargs = {
            'id': {'read_only': True},
            'name': {'read_only': True},
            'description': {'read_only': True},
            'email': {'read_only': True},
            'website': {'read_only': True},
            'logo': {'read_only': True},
            'photo': {'read_only': True},
            'latitude': {'read_only': True},
            'longitude': {'read_only': True},
            'street': {'read_only': True},
            'city': {'read_only': True},
            'state': {'read_only': True},
            'postal_code': {'read_only': True},
            'country': {'read_only': True},
        }
