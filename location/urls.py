from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()

urlpatterns = [
    path('api/location/', views.LocationView.as_view(), name='location'),
    path('api/location/<id>/', views.LocationUpdate.as_view(), name='edit'),
    path('api/location/<id>/delete', views.SoftDeleteLocationAPIView.as_view(),name='delete'),
    path('api/location/list', views.ListActiveLocationAPIView.as_view(), name='list_active'),
    path('api/location/pagination', views.ListPaginatedLocationAPIView.as_view(), name='pagination'),
    path('api/location/bar', views.LocationsLoggedBar.as_view(), name='list_per_bar'),
    path('api/location/bar/nosubscription', views.LocationsWithoutSubscriptionLogged.as_view(), name='list_per_bar_not_subscription')
]
