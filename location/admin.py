from django.contrib import admin
from .models import Location

class LocationAdmin(admin.ModelAdmin):
    list_display = ('name', 'city','street')
    ordering = ('name',)
    search_fields = ('name', 'city','street')
    list_filter = ('bar','is_active',)
    list_per_page = 10
    

admin.site.register(Location, LocationAdmin)
