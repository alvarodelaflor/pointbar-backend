from bar.models import Bar
from bar.permissions import IsAuthenticatedBar
from django.core.exceptions import ValidationError
from django.shortcuts import get_object_or_404
from employee.models import Employee
from rest_framework import generics, status
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from sale.models import Sale
from subscription.models import Subscription
from subscription.subscription_paypal_cancel import paypal_cancellation

from .models import Location
from .serializers import LocationSerializer


class LocationView(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticatedBar]
    serializer_class = LocationSerializer

    def get_queryset(self):
        user = self.request.user
        locations = []
        if(user.is_anonymous is False and user.authority == 'BAR'):
            bar_id = Bar.objects.get(user_account=self.request.user).id
            locations = Location.objects.filter(bar=bar_id, is_active=True)
        return locations

    def get(self, request, *args, **kwargs):
        self.serializer_class = LocationSerializer
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):

        st = status.HTTP_201_CREATED
        msg = 'ok, created'

        for data in ['name', 'description', 'email', 'website', 'logo', 'photo', 'latitude', 'longitude', 'street', 'city',
                     'state', 'postal_code', 'country']:
            if not data in request.data:
                return Response({}, status=status.HTTP_400_BAD_REQUEST)

        bar_id = Bar.objects.get(user_account=request.user).id

        try:
            bar = Bar.objects.get(pk=bar_id)
        except:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

        try:
            location = Location(
                name=request.data.get('name'),
                description=request.data.get('description'),
                email=request.data.get('email'),
                website=request.data.get('website'),
                logo=request.data.get('logo'),
                photo=request.data.get('photo'),
                latitude=request.data.get('latitude'),
                longitude=request.data.get('longitude'),
                street=request.data.get('street'),
                city=request.data.get('city'),
                state=request.data.get('state'),
                postal_code=request.data.get('postal_code'),
                country=request.data.get('country'),
                is_active=True,
                bar=bar)

            location.save()

        except:
            msg = 'some of atributes are invalid'
            st = status.HTTP_422_UNPROCESSABLE_ENTITY

        return Response(msg, status=st)


class LocationUpdate(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = LocationSerializer
    queryset = Location.objects.filter().all()
    permission_classes = [IsAuthenticatedBar]
    lookup_field = 'id'

    def put(self, request, id, *args, **kwars):
        location = get_object_or_404(Location, pk=id)
        
        st = status.HTTP_200_OK
        msg = 'updated'

        try:
            location.description = request.data.get('description')
            location.email = request.data.get('email')
            location.website = request.data.get('website')
            location.logo = request.data.get('logo')
            location.photo = request.data.get('photo')
            location.save()
        except:
            msg = 'some of atributes are invalid'
            st = status.HTTP_422_UNPROCESSABLE_ENTITY

        return Response(msg, status=st)

    def delete(self, request, location_id, *args, **kwars):
        location = get_object_or_404(Location, pk=location_id)
        st = status.HTTP_200_OK
        msg = 'deleted'

        try:
            location.delete()
        except:
            msg = 'cannot delete'
            st = status.HTTP_409_CONFLICT

        return Response(msg, status=st)


class SoftDeleteLocationAPIView(generics.DestroyAPIView):
    """
    Change is_active value to False for logic delete
    """

    serializer_class = LocationSerializer
    queryset = Location.objects.all()
    permission_classes = [IsAuthenticatedBar]
    lookup_field = 'id'

    def delete(self, request, id):

        location = self.queryset.get(id=id)
        location.is_active = False
        location.save()

        employees = Employee.objects.filter(location=location).all()
        for employee in employees:
            employee.location = None
            employee.granted_points = 0
            employee.control = False
            employee.save()

        sales = Sale.objects.filter(location=location).all()
        for sale in sales:
            sale.is_active = False
            sale.save()

        try:
            suscription = Subscription.objects.get(location=location)
            paypal_cancellation(suscription.paypal_subscription_id)
            suscription.is_active = False
            suscription.renovate = False
            suscription.save()
        except Subscription.DoesNotExist:
            msg = "no_sub_related"
            

        return Response(status=status.HTTP_204_NO_CONTENT)


class ListActiveLocationAPIView(generics.ListAPIView):
    """
    List active locations
    """

    permission_classes = [IsAuthenticatedBar]
    serializer_class = LocationSerializer

    def get_queryset(self):
        subs = Subscription.objects.filter(is_active=True).all()
        locs = []
        for sub in subs:
            locs.append(Location.objects.get(id=sub.location_id, is_active=True))
        queryset = locs
        return queryset


class LocationsLoggedBar(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticatedBar]
    serializer_class = LocationSerializer
    queryset = []

    def get_queryset(self):
        try:
            accountId = self.request.user.id
            bar = Bar.objects.get(user_account=accountId)
            queryset = Location.objects.filter(is_active=True, bar=bar)
        except:
            raise ValidationError('You can not see this locations')
        return queryset


class LocationsWithoutSubscriptionLogged(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticatedBar]
    serializer_class = LocationSerializer

    def get_queryset(self):
        queryset = []
        try:
            accountId = self.request.user.id
            bar = Bar.objects.get(user_account=accountId)
            locations = Location.objects.filter(is_active=True, bar=bar)
            for locationBar in locations:
                try:
                    subscription = Subscription.objects.get(
                        location_id=locationBar.id, is_active=True)
                except:
                    queryset.append(locationBar)

        except:
            raise ValidationError('You can not see this locations')
        return queryset


class SmallPagesPagination(PageNumberPagination):
    page_size = 8


class ListPaginatedLocationAPIView(generics.ListAPIView):
    """
    List active locations
    """
    pagination_class = SmallPagesPagination
    permission_classes = [IsAuthenticatedBar]
    serializer_class = LocationSerializer

    def get_queryset(self):
        user = self.request.user
        if(user.is_anonymous is False and user.authority == 'BAR'):
            bar_id = Bar.objects.get(user_account=self.request.user).id
            queryset = Location.objects.filter(bar=bar_id, is_active=True)
        return queryset
