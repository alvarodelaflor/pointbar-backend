from django.db import models
from bar.models import Bar
from django.contrib.postgres.fields import ArrayField

# Create your models here.

class Location(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=200)
    email = models.EmailField()
    website = models.URLField(blank='true',null='true')
    logo = models.TextField(max_length=1000000,blank='true',null='true')
    # photo = ArrayField(models.TextField(max_length=1000000,blank='true'),null='true')
    photo = ArrayField(models.URLField(max_length = 2048, blank='true'),null='true')
    latitude = models.FloatField()
    longitude = models.FloatField()
    street = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    postal_code = models.CharField(max_length=100)
    country = models.CharField(max_length=100)
    is_active = models.BooleanField(default=True)
    bar = models.ForeignKey(Bar, on_delete=models.CASCADE, related_name='barLocation')

    def __str__(self):
       return self.name