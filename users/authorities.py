from enum import Enum

class Authority(Enum):
    ADMIN = "ADMIN"
    BAR = "BAR"
    CLIENT = "CLIENT"
    EMPLOYEE = "EMPLOYEE"