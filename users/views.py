from django.contrib.auth import get_user_model
from django.http import JsonResponse
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_decode
from rest_framework import viewsets
from rest_framework_simplejwt.views import TokenObtainPairView

from .models import UserAccount
from .serializers import MyTokenObtainPairSerializer, UserSerializer
from .token import account_activation_token

UserModel = get_user_model()


class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = UserAccount.objects.all().order_by('username')
    serializer_class = UserSerializer

def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = UserAccount.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, UserAccount.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        data = {'success': True }
        return JsonResponse(data)
    else:
        data = {'success': False }
        return JsonResponse(data)
