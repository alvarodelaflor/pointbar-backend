from rest_framework import serializers
from django.core.validators import RegexValidator
from rest_auth.registration.serializers import RegisterSerializer
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from .models import UserAccount

class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)

        # Add custom claims
        token['username'] = user.username
        token['email'] = user.email
        token['authority'] = user.authority

        return token

class UserSerializer(RegisterSerializer):
    
    email = serializers.EmailField(required=True)
    authority = serializers.HiddenField(write_only=True, default=None, validators=[RegexValidator(regex='^BAR|CLIENT|EMPLOYEE$', message='The authority is incorrect')])

    def get_cleaned_data(self):
        data_dict = super().get_cleaned_data()
        data_dict['authority'] = self.validated_data.get('authority', '')
        return data_dict


class UserAccountDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserAccount
        fields = ('id', 'username', 'email', 'authority')
        read_only_fields = ('username', 'email', 'authority')
