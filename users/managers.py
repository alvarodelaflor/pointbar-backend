import re

from django.contrib.auth.base_user import BaseUserManager
from django.core.exceptions import ValidationError

from .authorities import Authority


class UserAccountManager(BaseUserManager):
    """
    Creación de un usuario en Pointbar donde el username es el identificador único
    """
    def create_user(self, username, password, email, authority):
        """
        Crear y guardar un usuario dados su correo y contraseña
        """

        authority_pattern = "^BAR|CLIENT|EMPLOYEE$"
        authority_matches_pattern = re.search(authority, authority_pattern)
        if authority_matches_pattern is None:
            raise ValidationError('The authority must be BAR or CLIENT or EMPLOYEE')

        if re.search(r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)', email) is None:
            raise ValidationError('The email does not seem to be correct')

        user = self.model(username=username, email=email, authority=authority)
        user.set_password(password)
        user.is_active = False
        user.save(using=self._db)

        return user

    def create_superuser(self, username, email, password):
        user = self.model(
            email=email,
            authority=Authority.ADMIN.value
        )
        user.username = username
        user.is_staff = True
        user.is_superuser = True
        user.set_password(password)
        user.save(using=self._db)
        return user
