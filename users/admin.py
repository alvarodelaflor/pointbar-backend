from django.contrib import admin, messages
from django.contrib.auth.admin import UserAdmin
from .models import UserAccount
from actor.models import Actor
from system_configuration.models import System_configuration
from pointbar_notifications.emails import send_mail
from pointbar_notifications.notifications import send_notification


class UserAccountAdmin(UserAdmin):
    list_per_page = 10


    def sendNotification(self, request, queryset):
        
        for user_account in queryset:
            if not(user_account.username == 'system') and not(user_account.email== '') and (user_account.is_active==True) and (not( 'AnonymousEmployee' in user_account.username or 'AnonymousBar' in user_account.username or 'AnonymousClient' in user_account.username)):
               send_notification(user_account, 'notification_security-breach', 'notification_security-breach_description')               
        
        msg = 'Notifications sended correctly'
        self.message_user(request, msg, messages.SUCCESS)
    sendNotification.short_description = "Send notification of security hole for selected users"

        
    def sendMail(self, request, queryset):
        system_configurationAll = System_configuration.objects.all()
        system_configuration=system_configurationAll[0]

        emails = []
        for user_account in queryset:
            if not(user_account.username == 'system') and not(user_account.email== '') and (user_account.is_active==True) and (not( 'AnonymousEmployee' in user_account.username or 'AnonymousBar' in user_account.username or 'AnonymousClient' in user_account.username)):
                emails.append(user_account.email)
        
        if len(emails)>50:
                msg = 'You can not send more of 50 emails at the same time. Please send this emails in diferents times'
                self.message_user(request, msg, messages.WARNING)
        else:
            
            try:
                send_mail(system_configuration.title_segurity, system_configuration.body_segurity, emails)
                msg = 'Emails sended correctly'
                self.message_user(request, msg, messages.SUCCESS)
            except:
                msg = 'The limit of daily emails is end, please contact with a superadmin for solve the problem, if you need to send this information rigth now you can use the PointBar notifications or send the emails with your corporative account. '
                self.message_user(request, msg, messages.WARNING)                       
        
        
    sendMail.short_description = "Send mail of security hole for selected users"

    def banUsers(self, request, queryset):

        for user_account in queryset:
            user_account.is_active=False
            user_account.save()
            try:
                actor= Actor.objects.get(user_account=user_account)
                actor.is_banned=True
                actor.save()
            except:
                pass
        msg = 'Users banned correctly'
        self.message_user(request, msg, messages.SUCCESS)    

    banUsers.short_description = "Ban selected users"        

    def unbanUsers(self, request, queryset):
        
        
        for user_account in queryset: 
            if not( 'AnonymousEmployee' in user_account.username or 'AnonymousBar' in user_account.username or 'AnonymousClient' in user_account.username):
                user_account.is_active=True
                user_account.save()
                try:
                    actor= Actor.objects.get(user_account=user_account)
                    actor.is_banned=False
                    actor.save()
                except:
                    pass        
        msg = 'Users unbanned correctly'
        self.message_user(request, msg, messages.SUCCESS) 

    unbanUsers.short_description = "Unban selected users"

    actions = [sendNotification,sendMail, banUsers,unbanUsers]

    def get_actions(self, request):
        actions = super().get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions


UserAdmin.fieldsets += (('Authority',{'fields' : ('authority',)}),)
admin.site.register(UserAccount, UserAccountAdmin)    