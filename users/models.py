from django.db import models
from django import forms
import re
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator


from .managers import UserAccountManager

class UserAccount(AbstractUser):
    
    email = models.EmailField(null=False, validators= [RegexValidator(regex='(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)', message='The email is not correct')])
    authority = models.CharField(max_length=8,help_text='BAR or ADMIN or CLIENT or EMPLOYEE', validators=[RegexValidator(regex='^BAR|ADMIN|CLIENT|EMPLOYEE$', message='The authority is incorrect')])
    
    REQUIRED_FIELDS = ['password', 'email']

    objects = UserAccountManager()
