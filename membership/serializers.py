from .models import Membership
from rest_framework import serializers


class MembershipSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Membership
        fields = ['id', 'titleS', 'descriptionS','titleE','descriptionE', 'price', 'membership_type', 'subscriptionId']
