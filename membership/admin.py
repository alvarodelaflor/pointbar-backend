from django.contrib import admin
from membership.models import Membership

class MembershipAdmin(admin.ModelAdmin):
    list_display = ('get_title', 'price', 'membership_type')
    ordering = ('price',)
    search_fields = ('titleE','price','membership_type')
    list_per_page = 10

    def get_readonly_fields(self, request, obj=None):
        if request.user.is_staff:
            if request.user.is_superuser:
                return []
            else:
                return ['membership_type','subscriptionId']

    def get_title(self, obj):
        return obj.titleE

    get_title.short_description = 'Title'




admin.site.register(Membership,MembershipAdmin)