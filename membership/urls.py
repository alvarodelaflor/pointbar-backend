from django.urls import include, path
from rest_framework import routers
from . import views

app_name = 'membership'

urlpatterns = [
    path('', views.MembershipView.as_view(), name='membership'),
]

