from django.conf import settings
from django.utils import timezone
from django.shortcuts import get_object_or_404
from rest_framework import generics, status
from rest_framework.response import Response
from .models import Membership
from bar.models import Bar
from .serializers import MembershipSerializer
from bar.permissions import IsAuthenticatedBar

class MembershipView(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticatedBar]
#    queryset = Membership.objects.all()
    serializer_class = MembershipSerializer

    def get_queryset(self):
        user = self.request.user
        memberships = []
        if(user.is_anonymous == False and user.authority == 'BAR'):
            memberships = Membership.objects
        else:
            memberships = Membership.objects
        return memberships
    
    def get(self, request, *args, **kwargs):
        self.serializer_class = MembershipSerializer
        return super().get(request, *args, **kwargs)