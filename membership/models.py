from django.db import models


class Membership(models.Model):

    titleS = models.CharField(('Title in Spanish'),max_length=100,blank=False, null=False)
    descriptionS = models.TextField(('Description in Spanish'),blank=False, null=False)
    titleE = models.CharField(('Title in English'),max_length=100,blank=False, null=False)
    descriptionE = models.TextField(('Description in English'),blank=False, null=False)
    price = models.PositiveIntegerField(blank=False, null=False)
    membership_type = models.CharField(max_length=100,help_text='Normal/VIP')
    subscriptionId = models.CharField(('Paypal Id'),max_length=756)

    def __str__(self):
       return self.titleE


