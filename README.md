[![pipeline status](https://gitlab.com/pointbar2/pointbar-backend/badges/develop/pipeline.svg)](https://gitlab.com/pointbar2/pointbar-backend/-/commits/develop)
[![coverage report](https://gitlab.com/pointbar2/pointbar-backend/badges/master/coverage.svg)](https://gitlab.com/pointbar2/pointbar-backend/-/commits/master)

# PointBar Backend

## Launch Django Rest Api

```python3 (or python) -m venv env```

---

**Windows**
```source env/Scripts/activate```

**Linux**
```source env/bin/activate```

---

```pip install -r requirements.txt```

```python manage.py migrate```

```python manage.py runserver```
