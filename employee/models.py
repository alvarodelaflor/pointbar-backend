from django.core.exceptions import ValidationError
from django.db import models
from actor.models import Actor
from bar.models import Bar
from location.models import Location


class Employee(Actor):
    is_working = models.BooleanField(default=False)
    location = models.ForeignKey(Location,on_delete=models.CASCADE,related_name='location',blank=True,null=True,default=None)
    bar_field = models.ForeignKey(Bar,on_delete=models.CASCADE,null=True,related_name='related_bar')
    control = models.BooleanField(default=False)
    limit_points = models.IntegerField(default=0)
    granted_points = models.IntegerField(default=0)

    def clean(self):

        bar_id = self.bar_field.id
        loc = self.location

        if self.is_working and (self.location is None or self.location == ''):
            raise ValidationError(
                'This employee must have a location if he is working')

        if not(loc is None or loc == '') and not(bar_id == self.location.bar.id):
            raise ValidationError('The bar must be owner of this location')

    def __str__(self):
        return self.user_account.username

    class Meta:
        proxy: True
