from rest_framework import permissions


class IsAuthenticatedEmployee(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.user.authority == 'EMPLOYEE' or request.user.is_superuser

