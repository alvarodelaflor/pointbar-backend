# Generated by Django 3.0.3 on 2020-04-06 21:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('location', '0004_auto_20200325_2333'),
        ('employee', '0006_auto_20200405_1757'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employee',
            name='location',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='location', to='location.Location'),
        ),
    ]
