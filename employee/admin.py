from django.contrib import admin
from .models import Employee

class EmployeeAdmin(admin.ModelAdmin):
    list_display = ('name', 'get_user_account', 'get_bar')
    ordering = ('inscriptionDate',)
    search_fields = ('name','user_account__username','bar_field__user_account__username')
    list_filter = ('location','is_banned')
    list_per_page = 10
    
    def get_user_account(self, obj):
        return obj.user_account

    get_user_account.short_description = 'User name'
    get_user_account.admin_order_field = 'employee__user_account'

    def get_bar(self, obj):
        return obj.bar_field

    get_bar.short_description = 'Bar'
    get_bar.admin_order_field = 'employee__bar_field'

admin.site.register(Employee,EmployeeAdmin)
