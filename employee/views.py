from rest_framework import generics, status, viewsets
from rest_framework.response import Response
from bar.models import Bar
from bar.permissions import IsAuthenticatedBar
from .permissions import IsAuthenticatedEmployee
from .models import Employee
from .models import Location
from users.models import UserAccount
from pointbar_notifications.notifications import send_notification
from .serializers import EmployeeSerializer
from .serializers import EmployeeGetSerializer
from rest_framework.pagination import PageNumberPagination
import random
import datetime
from rest_framework.decorators import api_view, permission_classes


class EmployeeViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    permission_classes = [IsAuthenticatedBar]
    queryset = Employee.objects.all().order_by('name')
    serializer_class = EmployeeSerializer


class EmployeeByUserAccountId(generics.ListCreateAPIView):
    serializer_class = EmployeeSerializer

    def get_queryset(self):
        queryset = []
        user = self.request.user
        employee = Employee.objects.get(user_account_id=user.id)
        queryset.append(employee)
        return queryset


class EmployeeGetPersonalInfo(viewsets.ModelViewSet):
    def get_queryset(self):
        user = self.request.user
        print(user)
        employee = Employee.objects.get(user_account_id=user.id)
        return [employee]

    serializer_class = EmployeeSerializer


class EmployeeUpdateViewSet(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticatedBar]
    serializer_class = EmployeeSerializer
    lookup_field = 'id'
    queryset = []

    def put(self, request, id):
        msg = 'updated'
        st = status.HTTP_200_OK
        try:
            employee = Employee.objects.get(id=id)
            employee.is_working = request.data.get('is_working')
            employee.granted_points = request.data.get('granted_points')
            employee.location = Location.objects.get(
                id=request.data.get('location'))
            employee.clean()
            employee.save()
        except:
            msg = 'some_of_atributes_are_invalid'
            st = status.HTTP_422_UNPROCESSABLE_ENTITY
        return Response(msg, status=st)


class EmployeeUpdateAPIView(generics.RetrieveUpdateAPIView):

    permission_classes = [IsAuthenticatedBar]
    serializer_class = EmployeeSerializer
    queryset = Employee.objects.all()
    lookup_field = 'id'

    def put(self, request, id):
        msg = 'updated'
        st = status.HTTP_200_OK
        try:
            employee = Employee.objects.get(id=id)

            employee.name = request.data.get('name')
            employee.is_working = request.data.get('is_working')
            employee.control = request.data.get('control')
            employee.limit_points = request.data.get('limit_points')
            employee.location = Location.objects.get(
                id=request.data.get('location'))

            employee.clean()
            employee.save()
        except:
            msg = 'some_of_atributes_are_invalid'
            st = status.HTTP_422_UNPROCESSABLE_ENTITY
        return Response(msg, status=st)


class EmployeeOwnUpdateAPIView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticatedEmployee]
    serializer_class = EmployeeGetSerializer

    def put(self, request):
        msg = "updated"
        st = status.HTTP_200_OK

        try:
            employee = Employee.objects.get(user_account_id=request.user.id)
            employee.name = request.data.get("name")
            employee.clean()
            employee.save()
        except:
            msg = "some_of_atributes_are_invalid"
            st = status.HTTP_422_UNPROCESSABLE_ENTITY

        return Response(msg, status=st)


class SmallPagesPagination(PageNumberPagination):
    page_size = 8


class EmployeesPerBarLogued(generics.ListCreateAPIView):

    """
    pagination_class = SmallPagesPagination

    """
    permission_classes = [IsAuthenticatedBar]
    serializer_class = EmployeeGetSerializer
    pagination_class = SmallPagesPagination
    queryset = []

    def get_queryset(self):
        accountId = self.request.user.id
        queryset = Employee.objects.filter(
            bar_field__user_account__id=accountId,
            user_account__is_active=True)
        return queryset


@api_view(["GET"])
@permission_classes((IsAuthenticatedEmployee,))
def deleteAnon(request):
    msg = "deleted"
    st = status.HTTP_200_OK

    try:
        user = UserAccount.objects.get(pk=request.user.id)
        emp_username = user.username
        emp = Employee.objects.get(user_account_id=request.user.id)
        randomEmpUsername = "AnonymousEmployee" + str(random.randrange(1000))
        while UserAccount.objects.filter(username=randomEmpUsername).exists():
            randomEmpUsername = "AnonymousEmployee" + str(
                random.randrange(1000)
            )
        user.username = randomEmpUsername
        user.email = randomEmpUsername + "@mail.com"
        user.is_active = False
        user.save()
        emp.name = randomEmpUsername
        emp.inscriptionDate = datetime.date.today()
        emp.is_banned = True
        emp.control = False
        emp.limit_points = 0
        emp.granted_points = 0
        emp.is_working = False
        emp.save()
        bar = Bar.objects.get(pk=emp.bar_field)
        barUA = UserAccount.objects.get(pk=bar.user_account_id)
        send_notification(barUA, 'employee_account_deleted___' +
                          str(emp_username), 'employee_account_deleted_description')
    except:
        msg = "delete_failed"
        st = status.HTTP_400_BAD_REQUEST

    return Response(msg, status=st)


@api_view(["GET"])
@permission_classes((IsAuthenticatedBar,))
def deleteBarAnon(request):
    msg = "deleted"
    st = status.HTTP_200_OK
    id = request.GET['id']

    try:
        user = UserAccount.objects.get(pk=request.user.id)
        emp = Employee.objects.get(id=id)  
        emp_user = UserAccount.objects.get(pk=emp.user_account.id)
        emp_username = emp_user.username 
        emp = Employee.objects.get(user_account_id=emp_user.id) 
        bar = Bar.objects.get(user_account_id=request.user.id)
        if not emp.bar_field.id == bar.id:
            raise AssertionError()
        randomEmpUsername = "AnonymousEmployee" + str(random.randrange(1000))
        while UserAccount.objects.filter(username=randomEmpUsername).exists():
            randomEmpUsername = "AnonymousEmployee" + str(
                random.randrange(1000)
            )
        emp_user.username = randomEmpUsername
        emp_user.email = randomEmpUsername + "@mail.com"
        emp_user.is_active = False
        emp_user.save()
        emp.name = randomEmpUsername
        emp.inscriptionDate = datetime.date.today()
        emp.is_banned = True
        emp.control = False
        emp.limit_points = 0
        emp.granted_points = 0
        emp.is_working = False
        emp.save()
        send_notification(user, 'employee_account_deleted___' +
                          str(emp_username), 'employee_account_deleted_description')
    except:
        msg = "delete_failed"
        st = status.HTTP_400_BAD_REQUEST

    return Response(msg, status=st)
