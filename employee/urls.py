from django.urls import path
from employee import views

app_name = 'employee'

urlpatterns = [
    path('<id>/edit', views.EmployeeUpdateAPIView.as_view(), name='edit'),
    path('myemployee', views.EmployeesPerBarLogued.as_view(), name='myemployee'),
    path('own/update/', views.EmployeeOwnUpdateAPIView.as_view(), name='updateEmployeeData'),
]
