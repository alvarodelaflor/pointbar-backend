from bar.models import Bar
from django.core.exceptions import ValidationError
from django.db import transaction
from location.models import Location
from pointbar_notifications.emails import email_verification
from rest_framework import serializers
from users.authorities import Authority
from users.models import UserAccount
from users.serializers import UserSerializer

from .models import Employee


class EmployeeSerializer(serializers.ModelSerializer):
    user_account = UserSerializer(required=True)
    terms_and_conditions = serializers.SerializerMethodField()

    def create(self, validated_data):
        with transaction.atomic():
            user_data = validated_data.pop('user_account')
            user = UserAccount.objects.create_user(
                user_data['username'], user_data['password1'], user_data['email'], Authority.EMPLOYEE.value)
            is_working = validated_data.pop('is_working')
            control = validated_data.pop('control')
            limit_points = validated_data.pop('limit_points')
            granted_points = 0

            if self.get_terms_and_conditions(self) != "True":
                raise ValidationError(
                    'You must accept our terms and conditions')

            employee, created = Employee.objects.update_or_create(
                user_account=user,
                name=validated_data.pop('name'),
                is_working=is_working,
                control=control,
                limit_points=limit_points,
                granted_points=granted_points,
                location=validated_data.pop('location'))

            email_verification(user)

            user = self.context['request'].user

            logged_bar = Bar.objects.get(user_account_id=user.id)

            employee.bar_field = logged_bar
            employee.clean()
            employee.save()

        return employee

    def get_terms_and_conditions(self, obj):
        try:
            return self.context['request'].data['terms_and_conditions']
        except TypeError:
            return "terms_and_conditions does not exists"
        except:
            pass

    class Meta:
        model = Employee
        fields = ['id', 'user_account', 'name', 'is_working',
                  'control', 'limit_points', 'granted_points',
                  'location', 'bar_field', 'terms_and_conditions']


class locationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Location
        fields = ('id', 'logo', 'name')


class barSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Bar
        fields = ('id',)


class EmployeeGetSerializer(serializers.HyperlinkedModelSerializer):

    location = locationSerializer(many=False)
    bar_field = barSerializer(many=False)
    user_account = UserSerializer(many=False)

    class Meta:
        model = Employee
        fields = ['id', 'user_account', 'name', 'is_working',
                  'control', 'limit_points', 'granted_points',
                  'location', 'bar_field']
