from django.contrib import admin
from .models import Client
# Register your models here.

class ClientAdmin(admin.ModelAdmin):
    list_display = ('name', 'get_user_account','birth_date', 'inscriptionDate')
    ordering = ('inscriptionDate','birth_date')
    search_fields = ('name','user_account__username')
    list_filter = ('is_banned',)
    list_per_page = 10
    
    def get_user_account(self, obj):
        return obj.user_account

    get_user_account.short_description = 'User name'
    get_user_account.admin_order_field = 'client__user_account'

admin.site.register(Client, ClientAdmin)
