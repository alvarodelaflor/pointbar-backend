from rest_framework import permissions

class IsAuthenticatedClient(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.user.authority == 'CLIENT' or request.user.is_superuser
