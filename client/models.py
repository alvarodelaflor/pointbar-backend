import re
from datetime import date, datetime

from django.core.exceptions import ValidationError
from django.db import models
from phone_field import PhoneField

from actor.models import Actor

# Create your models here.

class Client(Actor):
    surname = models.CharField(max_length=100)
    birth_date = models.DateField()
    phone = models.TextField()
    # photo = models.ImageField(upload_to='client/images', null=True)
    photo = models.TextField(blank='true',null='true')
    # photo = models.URLField(max_length = 2048, blank='true',null='true')

    def __str__(self):
       return self.user_account.username

    def clean(self):
        if re.search(r'^([0-9]{9})$|((\+)?[0-9]{11})$|((\()?[0-9]{3}(\))? [0-9]{3}-[0-9]{4})$', self.phone) is None:
            raise ValidationError('The phone number does not seem to match the pattern')
        
        try:
            birth_date_received = datetime.strptime(self.birth_date, '%Y-%m-%d').date()
        except TypeError: # birth_date comes from admin form
            birth_date_received = self.birth_date
        
        if (birth_date_received > date.today()):
                raise ValidationError('The birth date must be in the past')

        # if (datetime.strptime(self.birth_date, "%Y-%m-%d").date() > date.today()):
        #     raise ValidationError('The birth date must be in the past')

    class Meta:
        proxy: True
