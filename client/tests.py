from django.test import TestCase
from django.urls import include, path, reverse
from rest_framework.test import APITestCase, URLPatternsTestCase
from rest_framework import status
from users.models import UserAccount
from client.models import Client
from rest_framework.test import APIClient
from users.authorities import Authority
import datetime



class API_client_test(APITestCase, URLPatternsTestCase):
    urlpatterns = [path('', include('pointbar.urls')),]

    def setUp(self):
        user = UserAccount.objects.create_user('ClienteEjemplo1', 'ClienteEjemplo1', 'perico12345@gmail.com', Authority.CLIENT.value)
        user.is_active=True
        user.save()
        date_str = "2001-12-31"
        birth_date = datetime.datetime.strptime(date_str, "%Y-%m-%d").date()

        client = Client(
            name = 'Julian',
            is_banned = False,
            user_account = user,
            surname = 'Garcia',
            birth_date = birth_date,
            phone= '672108122')
        client.save()

        super().setUp()

    def tearDown(self):
        super().tearDown()

    def client_logged(self,json_user_data):

        url = reverse('token_obtain_pair')
        response = self.client.post(url,json_user_data, format='json')
        token=response.data['access']
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Bearer ' + token)
        return client
  
    
    def test_create_and_delete_client_account(self):
        
        user_data= {
                "username": "ClienteEjemplo1",
                "password": "ClienteEjemplo1"
              }

        client= API_client_test.client_logged(self,user_data)

        url = reverse('deleteClient')
        data = {}
        response = client.post(url,data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

