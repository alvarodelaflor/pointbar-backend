import re
from datetime import date

from django.core.exceptions import ValidationError
from django.db import transaction
from pointbar_notifications.emails import email_verification
from pointbar_notifications.notifications import send_notification
from rest_framework import serializers
from users.authorities import Authority
from users.models import UserAccount
from users.serializers import UserSerializer

from .models import Client


class ClientSerializer(serializers.ModelSerializer):
    user_account = UserSerializer(required=True)
    terms_and_conditions = serializers.SerializerMethodField()

    def create(self, validated_data):
        with transaction.atomic():
            user_data = validated_data.pop('user_account')
            user = UserAccount.objects.create_user(user_data['username'], user_data['password1'], user_data['email'], Authority.CLIENT.value)
            phone = validated_data.pop('phone')
            birth_date = validated_data.pop('birth_date')

            if re.search(r'^([0-9]{9})$|((\+)?[0-9]{11})$|((\()?[0-9]{3}(\))? [0-9]{3}-[0-9]{4})$', phone) is None:
                raise ValidationError('The phone number does not seem to match the pattern')

            birth_date_format = '%Y-%m-%d'

            if (birth_date > date.today()):
                raise ValidationError('The birth date must be in the past')

            if self.get_terms_and_conditions(self) != "True":
                raise ValidationError('You must accept our terms and conditions')

            client, created = Client.objects.update_or_create(user_account=user,
                                name=validated_data.pop('name'),
                                surname=validated_data.pop('surname'),
                                birth_date=birth_date,
                                phone=phone,
                                photo=validated_data.pop('photo'))

            email_verification(user)
            send_notification(user, 'notification_register')
        return client

    def get_terms_and_conditions(self, obj):
        try:
            return self.context['request'].data['terms_and_conditions']
        except TypeError:
            return "terms_and_conditions does not exists"
        except:
            pass

    class Meta:
        model = Client
        fields = ['id', 'user_account', 'name', 'surname', 'birth_date', 'phone', 'photo', 'terms_and_conditions']
