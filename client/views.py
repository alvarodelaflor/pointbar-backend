from django.shortcuts import render
from rest_framework import generics, status, viewsets
from rest_framework.response import Response

from .models import Client
from .permissions import IsAuthenticatedClient
from .serializers import ClientSerializer
from users.models import UserAccount
import random
import datetime
from rest_framework.decorators import api_view, permission_classes
from rest_framework.pagination import PageNumberPagination
from operator import attrgetter
from django.db.models import Q

# Create your views here.

class ClientViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Client.objects.all().order_by('name')
    serializer_class = ClientSerializer

class ClientByUserAccountId(generics.ListCreateAPIView):
    serializer_class = ClientSerializer

    def get_queryset(self):
        queryset = []
        userAcoountId = self.request.GET.get('id')
        client = Client.objects.get(user_account = userAcoountId)
        queryset.append(client)
        return queryset
        
class ClientGetPersonalInfo(viewsets.ModelViewSet):
    def get_queryset(self):
        user = self.request.user
        client = Client.objects.get(user_account_id=user.id)
        return [client]

    serializer_class = ClientSerializer

class ClientUpdateViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticatedClient]
    serializer_class = ClientSerializer
    def put(self, request):
        msg = 'updated'
        st = status.HTTP_200_OK
        
        try:
            client = Client.objects.get(user_account_id=request.user.id)
            client.name = request.data.get('name')
            client.surname = request.data.get('surname')
            client.birth_date = request.data.get('birth_date')
            client.phone = request.data.get('phone')
            client.photo = request.data.get('photo')
            client.clean()
            client.save()
        except:
            msg = 'some_of_atributes_are_invalid'  
            st = status.HTTP_422_UNPROCESSABLE_ENTITY

        return Response(msg, status=st)

@api_view(["POST"])
# @permission_classes((IsAuthenticatedClient,))
def deleteAnon(request):
        msg = 'deleted'
        st = status.HTTP_200_OK
        
        try:
            client = Client.objects.get(user_account_id=request.user.id)
            user = UserAccount.objects.get(pk=request.user.id)
            randomUsername = "AnonymousClient"+str(random.randrange(1000))
            while UserAccount.objects.filter(username=randomUsername).exists():
                randomUsername = "AnonymousClient"+str(random.randrange(1000))
            user.username = randomUsername
            user.email = randomUsername + "@mail.com"
            user.is_active = False
            user.save()
            client.name = randomUsername
            client.inscriptionDate = datetime.date.today()
            client.phone = '000000000'
            client.surname = "Anon"
            client.photo = 'No photo'
            client.birth_date = datetime.date(1990, 1, 1)
            client.is_banned = True
            client.save()
            # client = Client.objects.get(user_account_id=request.user.id)
            # client.photo = 'No photo'
            # client.save()
        except:
            msg = 'delete_failed'  
            st = status.HTTP_400_BAD_REQUEST

        return Response(msg, status=st)

class SmallPagesPagination(PageNumberPagination):  
    page_size = 8
    
class getAllClient(generics.ListCreateAPIView):
    serializer_class = ClientSerializer
    pagination_class = SmallPagesPagination

    def get_queryset(self):
        queryset = []
        username = self.request.GET.get('username')

        queryset = Client.objects.all()

        if not (username == "none"):
            queryset = queryset.filter(user_account__username__icontains = username, user_account__is_active = True)

        return queryset