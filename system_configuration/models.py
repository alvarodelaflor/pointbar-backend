from django.db import models
from django.core.exceptions import ValidationError


class System_configuration(models.Model):
    
    title_verify= models.CharField(('Title of mail of account verification'),max_length=60,blank=False, null=False,help_text='verify your account/verifique su cuenta')
    body_verify=models.TextField(('Body of mail of account verification'),max_length=600,blank=False, null=False,help_text='verify your account/verifique su cuenta')
    title_export_datas_S = models.CharField(('Title in Spanish of mail of export datas'),max_length=60,blank=False, null=False)
    title_export_datas_E = models.CharField(('Title in English of mail of export datas'),max_length=60,blank=False, null=False)
    body_export_datas_S = models.TextField(('Body in Spanish of mail of export datas'),max_length=250,blank=False, null=False)
    body_export_datas_E = models.TextField(('Body in English of mail of export datas'),max_length=250,blank=False, null=False)
    title_segurity= models.CharField(('Title of mail of security hole'),max_length=60,blank=False, null=False,help_text='security hole/agujero de seguridad')
    body_segurity = models.TextField(('Body of mail of security hole'),max_length=600,blank=False, null=False,help_text='security hole/agujero de seguridad')

    def __str__(self):
       return 'System configuration'

    class Meta:
        verbose_name = 'System configuration'
        verbose_name_plural = 'System configurations'   

    def clean(self): 
        
        try:
            system_configuration = System_configuration.objects.all()
            vacio=not system_configuration.exists()
  
            if not vacio:
                if not (system_configuration[0].id==self.id):
                    raise ValidationError('There can only be one system configuration')
        except:
            raise ValidationError('There can only be one system configuration')
       